import React, { Component } from "react";
import {
  Image,
  Text,
  View,
  PermissionsAndroid,
  Dimensions,
} from "react-native";
import colors from "./../../assets/colors";
import st from "./../../assets/styles";
import moment from "moment";
import Geolocation from "@react-native-community/geolocation";
import Geocoder from "react-native-geocoding";

import {
  Container,
  Content,
  Card,
  CardItem,
  Icon,
  Body,
  Thumbnail,
} from "native-base";

import ArchHeader from "./../../components/ArchHeader";
import { inject, observer } from "mobx-react";
import database from "@react-native-firebase/database";
import { TouchableOpacity } from "react-native-gesture-handler";

class Event extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      events: [],
    };
    this.getEvents();
  }

  getEvents = () =>
    global.defaultMasjidName &&
    database()
      .ref("events")
      .orderByChild("selectedMasjidName")
      .equalTo(global.defaultMasjidName)
      .on("value", (snapshot) => {
        if (snapshot._snapshot.exists) {
          const values = snapshot.val();
          const events = [];
          Object.keys(values).map((e) => {
            values[e].id = e;
            events.push(values[e]);
            console.log(values[e]);
          });
          this.setState({ events });
        }
      });

  render() {
    const { getTranslatedString } = this.props.store.strings;
    return (
      <Container>
        <ArchHeader
          {...this.props}
          title={getTranslatedString("Masjid Events & Announcements")}
          isLoading={this.state.isLoading}
        />
        <Content style={{ backgroundColor: colors.backgroundColor }}>
          {this.state.events.map((e) => (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("EventDetail", {
                  eventData: e,
                })
              }
            >
              <Card
                transparent
                style={{
                  marginTop: 0,
                  marginLeft: 0,
                  marginRight: 0,
                  marginBottom: 10,
                }}
              >
                <CardItem cardBody>
                  <Thumbnail
                    large
                    source={{
                      uri: e.imageUrl,
                    }}
                    style={{
                      height: 200,
                      borderRadius: 0,
                      width: Dimensions.get("window").width,
                    }}
                  />
                </CardItem>
                <CardItem>
                  <Body
                    style={{
                      borderBottomWidth: 2,
                      paddingBottom: 10,
                      borderBottomColor: colors.grey,
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 25,
                        fontWeight: "bold",
                        marginBottom: 20,
                      }}
                    >
                      {e.name}
                      {/* {getTranslatedString("Upgrad.ID Event")} */}
                    </Text>
                    <View
                      style={{ flexDirection: "row", alignItems: "center" }}
                    >
                      <Icon
                        name="time"
                        style={{ fontSize: 20, color: colors.primaryColor }}
                      />
                      <Text style={{ paddingLeft: 5 }}>
                        {`${new Date(e.eventDate).toDateString()}, ${
                          e.eventTime
                        }`}
                        {/* {getTranslatedString("Monday, 7th Oct 7:45pm")} */}
                      </Text>
                    </View>
                    <View
                      style={{ flexDirection: "row", alignItems: "center" }}
                    >
                      <Icon
                        name="pin"
                        style={{
                          fontSize: 20,
                          color: colors.primaryColor,
                          paddingLeft: 2,
                        }}
                      />
                      <Text style={{ paddingLeft: 5 }}>{e.location}</Text>
                    </View>
                  </Body>
                </CardItem>
              </Card>
            </TouchableOpacity>
          ))}
        </Content>
      </Container>
    );
  }
}

export default inject("store")(observer(Event));
