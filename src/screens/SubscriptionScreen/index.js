import React, { PureComponent } from 'react'
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Platform,
    Alert,
    ActivityIndicator
} from 'react-native'
import { Container } from 'native-base';

import RNIap, {
    purchaseUpdatedListener,
    purchaseErrorListener,
    consumePurchaseAndroid,
    finishTransactionIOS
} from 'react-native-iap';

import ArchHeader from '../../components/ArchHeader';
import colors from '../../assets/colors';
import { observer, inject } from 'mobx-react';


const itemSkus = Platform.select({
    ios: [
        'standard_subscription',
        'premium_subscription'
    ],
    android: [
        // 'standard_subscription',
        // 'expert_subscription',
        'android.test.purchased',
        'android.test.canceled',
        'android.test.refunded',
        'android.test.item_unavailable'
    ],
});

class SubscriptionScreen extends PureComponent {
    state = {
        indicator: false,
        subscribed: false
    }

    componentDidMount = async () => {
        try {
            this.purchaseUpdateSubscription = purchaseUpdatedListener(
                async (purchase) => {
                    try {
                        // console.log('purchaseUpdatedListener');
                        // console.table(purchase);

                        Alert.alert(
                            'Success!',
                            'You are successfully subscribed to monthly subscription for 1 year'
                        );

                        this.setState({ subscribed: true });

                        let consumePurchaseResult;

                        if (Platform.OS === 'android') {
                            consumePurchaseResult = await consumePurchaseAndroid(purchase.purchaseToken);
                        } else {
                            consumePurchaseResult = await finishTransactionIOS();
                        }

                        console.log('consumePurchaseResult result is: ', consumePurchaseResult);

                        // this.setState({ indicator: false });
                    } catch (error) {
                        console.log('Buy subscription API error: ', error);
                        this.setState({ indicator: false });
                        Alert.alert('Error!', error);
                    }
                });
            this.purchaseErrorSubscription = purchaseErrorListener(
                (error) => {
                    this.setState({ indicator: false });
                    console.log('Premium purchaseErrorListener: ', error);
                });
        } catch (error) {
            console.log('RNIAP listner error: ', error);
            this.setState({ indicator: false });
        }
    }

    componentWillUnmount() {
        if (this.purchaseUpdateSubscription) {
            this.purchaseUpdateSubscription.remove();
            this.purchaseUpdateSubscription = null;
        }
        if (this.purchaseErrorSubscription) {
            this.purchaseErrorSubscription.remove();
            this.purchaseErrorSubscription = null;
        }
        if (Platform.OS === 'android') {
            RNIap.endConnectionAndroid()
        }
    }

    configueIAPAndRequestPayment = async () => {
        try {
            this.setState({ indicator: true });
            const rnInitialized = await RNIap.initConnection();
            if (rnInitialized) {    //react-native-iap initialized
                //to get all available products
                const products = await RNIap.getProducts(itemSkus);

                console.log('Expert all products are: ', products);
                /* 
                //to get all available purchases to be consumed
                const availableSubscriptions = await RNIap.getSubscriptions(itemSkus);

                console.log('available Subscriptions are: ', availableSubscriptions)
                if (availableSubscriptions.length > 0) {
                    //to consume all pending purchases
                    await RNIap.consumeAllItemsAndroid();
                    console.log('all items consumed successfully')
                } */

                //to request Standard Subscriptions
                let subscription = Platform.OS === 'ios' ? 'premium_subscription' : 'android.test.purchased';
                this.requestSubscription(subscription);
            }
        } catch (err) {
            console.log("In app error: ", err)
            this.setState({ indicator: false });
        }
    }

    requestSubscription = async (sku) => {
        try {
            console.log('requested subscription is: ', sku)
            RNIap.requestSubscription(sku);
        } catch (err) {
            console.log('RNIAP error: ', err)
        }
    }


    render() {
        const { subscribed, indicator } = this.state;
		const { getTranslatedString } = this.props.store.strings;

        return (
            <Container>
                <ArchHeader
                    {...this.props}
                    title={getTranslatedString("Subscriptions")}
                />

                <View style={styles.container}>
                    {subscribed ?
                        <Text style={styles.doNotHaveSubscription}>
                            {getTranslatedString('You are subscribed to monthly subscription of $1 for 1 month.')}
                        </Text>
                        :
                        <>
                            <Text style={styles.doNotHaveSubscription}>
                                {getTranslatedString('Currently you do not have any subscription')}
                            </Text>

                            <TouchableOpacity
                                activeOpacity={0.6}
                                style={styles.subscriptionButton}
                                onPress={() => this.configueIAPAndRequestPayment()}
                            >
                                <Text style={styles.buttonLabel}>
                                    {getTranslatedString('Get subscription for 1 month')}
                                </Text>
                            </TouchableOpacity>
                        </>
                    }
                </View>
                {indicator ?
                    <View style={styles.loader}>
                        <ActivityIndicator
                            color={colors.backgroundColor}
                        />
                    </View>
                    : null
                }
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 15
    },
    doNotHaveSubscription: {
        fontSize: 14
    },
    subscriptionButton: {
        height: 46,
        backgroundColor: colors.primaryColor,
        justifyContent: 'center',
        paddingHorizontal: 15,
        marginTop: 15
    },
    buttonLabel: {
        color: colors.backgroundColor,
        fontSize: 16
    },
    loader: {
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default inject('store')(observer(SubscriptionScreen));
