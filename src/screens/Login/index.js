import auth, { firebase } from "@react-native-firebase/auth";
import database from "@react-native-firebase/database";
import { inject, observer } from "mobx-react";
import { Button } from "native-base";
import React, { Component } from "react";
import { Image, StatusBar, Text, View, Dimensions, Alert } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import st from "./../../assets/styles";
import ArchInput from "./../../components/ArchInput";
import ArchRoundedButton from "./../../components/ArchRoundedButton";
import SocialButton from "./../../components/SocialButton";

import store from "../../mst/stores";
import userActions from "../../mst/actions/userActions";

import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes
} from "@react-native-community/google-signin";
import {
  AccessToken,
  LoginManager,
  LoginButton,
  ShareDialog
} from "react-native-fbsdk";
import messaging from "@react-native-firebase/messaging";

const { width, height } = Dimensions.get("window");
//morutabana69@gmail.com, #Loveathai123
const SHARE_LINK_CONTENT = {
  contentType: "link",
  contentUrl: "https://www.facebook.com/"
};
//https://wootoop.com/privacy-policy/
class LoginScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      error: "",
      logintype: "",
      gettingLoginStatus: false,
      userInfo: null,
      photourl: "",
      uid: ""
    };
    var outstr = global.logout;
    console.log("============", outstr);
  }
  componentWillMount = () =>
    messaging()
      .getToken()
      .then(e => (this.notificationToken = e))
      .catch(e => console.log("notification token err", e));
  componentDidMount() {
    GoogleSignin.configure({
      // scopes : ['https://www.googleapis.com/auth/drive.readonly'],
      // forceConsentPrompt : true,
      // webClientId: '440162861781-diseu9iksi5c3idfg1tq01cvped8pnhq.apps.googleusercontent.com'
      webClientId:
        "1070901058566-a4c6c56ejli3fj4m18seopv2rffq3bac.apps.googleusercontent.com"
    });
    this._isSignedIn();
  }

  updateUserNotificationToken = async () => {
    const uid = auth().currentUser.uid;
    const ref = database().ref(`/users/${uid}`);
    // const userData = await (await ref.once("value")).val();
    // const notificationToken = [];
    // console.log("token2:=>", userData.notificationToken);
    // if (userData.notificationToken) {
    //   console.log("notificationToken 0:=>", notificationToken);
    //   notificationToken.concat(userData.notificationToken);
    //   if (this.notificationToken) {
    //     if (!notificationToken.find((e) => e === this.notificationToken)) {
    //       notificationToken.push(this.notificationToken);
    //     }
    //   }
    //   console.log("notificationToken 1:=>", notificationToken);
    // } else if (this.notificationToken) {
    //   notificationToken.push(this.notificationToken);
    // }
    // console.log("last notificationToken:=>", notificationToken);
    ref.update({
      notificationToken: this.notificationToken ? this.notificationToken : null
    });
  };

  _isSignedIn = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    if (isSignedIn) {
      this._getCurrentUserInfo();
    } else {
      console.log("Please Login");
    }
    this.setState({ gettingLoginStatus: false });
  };

  _getCurrentUserInfo = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      global.name = userInfo.user.givenName + " " + userInfo.user.familyName;
      global.email = userInfo.user.email;
      global.photoUrl = userInfo.user.photo;
      global.id = userInfo.user.id;
      global.uid = userInfo.user.id;
      global.role = userInfo.user.role;
      this.props.navigation.navigate("MainApp");
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_REQUIRED) {
        alert("User has not signed in yet");
        console.log("User has not signed in yet");
      } else {
        alert("Something went wrong. Unable to get user's info");
        console.log("Something went wrong. Unable to get user's info");
      }
    }
  };

  ongooglesignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices({
        showPlayServicesUpdateDialog: true
      });

      const userInfo = await GoogleSignin.signIn();

      if (userInfo) {
        global.name = userInfo.user.givenName + " " + userInfo.user.familyName;
        global.email = userInfo.user.email;
        global.photoUrl = userInfo.user.photo;
        global.id = userInfo.user.id;
        global.uid = userInfo.user.id;
        global.role = userInfo.user.role;
        this.props.navigation.navigate("MainApp");
      }
    } catch (error) {
      Alert.alert("Google Signin error", JSON.stringify(error));
      console.log("Message", error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log("User Cancelled the Login Flow");
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log("Signing In");
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log("Play Services Not Available or Outdated");
      } else {
        console.log("Some Other Error Happened");
      }
    }
  };

  onGoogleLogin() {
    this.ongooglesignIn();
  }

  _isSignedInFB() {}

  facebookLogin() {
    // console.log("facebook login : ");
    // LoginManager.getLoginBehavior().then((res)=>{
    //   console.log(res);
    //   if(res.status === 'connected' )
    //   {
    //     try{
    //       LoginManager.logOut()

    //     } catch (error) {
    //       console.log(error);
    //     }
    //   }

    // })

    LoginManager.logInWithPermissions(["public_profile"]).then(
      result => {
        console.log(result);
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          AccessToken.getCurrentAccessToken().then(data => {
            console.log(data);
            const { accessToken } = data;
            this.initUser(accessToken);
          });
          console.log(
            "Login success with permissions: " +
              result.grantedPermissions.toString()
          );
          this.props.navigation.navigate("MainApp");
        }
      },
      function(error) {
        console.log("Login fail with error: " + error);
      }
    );
  }

  initUser(token) {
    fetch(
      "https://graph.facebook.com/v2.5/me?fields=id,email,name,picture.height(480),friends&access_token=" +
        token
    )
      .then(function(response) {
        console.log(response);
        return response.json();
      })
      .then(function(json) {
        global.email = json.email;
        global.id = json.id;
        global.uid = json.id;
        global.name = json.name;
        global.photoUrl = json.picture.data.url;
      })
      .catch(() => {
        reject("ERROR GETTING DATA FROM FACEBOOK");
      });
  }

  onFaceBookLogin() {
    this.facebookLogin();
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar backgroundColor="#004e00" barStyle="light-content" />
        <LinearGradient
          colors={["#004e00", "#00c004", "#001900"]}
          style={st.centerView}
        >
          <View style={st.contentForm}>
            <Image
              source={require(`./../../assets/images/logo.png`)}
              resizeMode="contain"
              style={st.logoImage}
            />

            <View style={st.separator} />

            <ArchInput
              name="email"
              onchangetext={text => {
                this.setState({ email: text });
              }}
              keyboardType={"email-address"}
            />
            <ArchInput
              name="password"
              onchangetext={text => {
                this.setState({ password: text });
              }}
            />
            <View style={{ flexDirection: "row" }}>
              <Button transparent>
                <Text style={{ color: "white" }}>Forgot your password?</Text>
              </Button>
              <Button
                transparent
                style={{ marginLeft: 5 }}
                onPress={() => {
                  this.props.navigation.navigate("ResetPassword");
                }}
              >
                <Text style={{ color: "white", fontWeight: "bold" }}>
                  Click Here
                </Text>
              </Button>
            </View>

            <ArchRoundedButton
              text="LOGIN"
              navigation={this.props.navigation}
              onpress={() => {
                if (this.state.email && this.state.password) {
                  this.setState({ error: "" });
                  auth()
                    .signInWithEmailAndPassword(
                      this.state.email,
                      this.state.password
                    )
                    .then(res => {
                      this.updateUserNotificationToken();
                      global.email = this.state.email;
                      global.photoUrl = res.user.photoURL;
                      global.id = res.user.uid;
                      global.uid = res.user.uid;
                      global.name = res.user.displayName;
                      // console.log("==================", res.user.displayName, res.user.email, res.user.uid, res.user.photoURL);

                      this.props.navigation.navigate("MainApp");
                    })
                    .catch(error => {
                      console.log("Firebase error: ", error);
                      let err = error;
                      err = err.split("] ")[1];
                      this.setState({ error: err });
                    });
                } else {
                  this.setState({
                    error: "Kindly provide email address and password"
                  });
                }
              }}
            />
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: width - 20
              }}
            >
              <SocialButton
                text="GOOGLE LOGIN"
                navigation={this.props.navigation}
                onpress={() => this.onGoogleLogin()}
              />
              <SocialButton
                text="FACEBOOK LOGIN"
                navigation={this.props.navigation}
                onpress={() => this.onFaceBookLogin()}
              />
            </View>
            <View style={{ flexDirection: "row", marginTop: 50 }}>
              <Button transparent>
                <Text style={{ color: "white" }}>New here?</Text>
              </Button>
              <Button
                transparent
                style={{ marginLeft: 5 }}
                onPress={() => this.props.navigation.navigate("Register")}
              >
                <Text style={{ color: "white", fontWeight: "bold" }}>
                  Register Now
                </Text>
              </Button>
            </View>
            {this.state.error ? (
              <Text style={{ color: "white", paddingTop: 15 }}>
                {this.state.error}
              </Text>
            ) : null}
          </View>
        </LinearGradient>
      </View>
    );
  }
}

export default inject("store")(observer(LoginScreen));
