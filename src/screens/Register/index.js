import { inject, observer } from "mobx-react";
import { Button, Thumbnail } from "native-base";
import React, { Component, Fragment } from "react";
import { images } from "../../assets/images";
import {
  ScrollView,
  StatusBar,
  Text,
  View,
  CheckBox,
  TouchableOpacity,
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import st from "./../../assets/styles";
import ArchInput from "./../../components/ArchInput";
import ArchRoundedButton from "./../../components/ArchRoundedButton";

import auth from "@react-native-firebase/auth";
import database from "@react-native-firebase/database";
import storage from "@react-native-firebase/storage";
import messaging from "@react-native-firebase/messaging";
import ImagePicker from "react-native-image-picker";
import loader from "../../assets/images/ellipsis.gif";

class RegisterScreen extends Component {
  componentWillMount = () =>
    messaging()
      .getToken()
      .then((e) => (this.notificationToken = e))
      .catch((e) => console.log("notification token err", e));

  componentDidMount() {}
  state = {
    error: "",
    username: "",
    email: "sdfsdfsdf",
    location: "",
    mobile: "",
    address: "",
    password: "sdfsdfadsfads",
    newPassword: "",
    check: false,
    role: "user",
    photoUrl: "",
  };
  handleUserIcon = () => {
    const options = {
      title: "Select Image",
      storageOptions: {
        skipBackup: true,
        path: "images",
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log("Response = ", response);
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        this.uploadUserIcon(response.uri);
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
      }
    });
  };
  uploadUserIcon = async (filePath) => {
    this.setState({ imgloader: true });
    const reference = storage().ref(`/user/${new Date().toJSON()}.jpeg`);
    await reference
      .putFile(filePath)
      .then(async (e) => {
        const url = await reference.getDownloadURL();
        global.photoUrl = url;
        console.log(url);
        this.setState({ imgloader: false, photoUrl: url });
      })
      .catch((e) => console.log("err", e));
  };
  storeUserInfo = async () => {
    const uid = auth().currentUser.uid;
    const createdAt = auth().currentUser.metadata.creationTime;
    const ref = database().ref(`/users/${uid}`);
    await ref.set({
      isBroadcastNotification: true,
      isEventNotification: true,
      isPrayerNotification: true,
      photoUrl: this.state.photoUrl,
      uid,
      name: this.state.username,
      email: this.state.email,
      cell: this.state.mobile,
      location: this.state.location,
      address: this.state.address,
      createdAt: createdAt,
      role: this.state.role,
      notificationToken: this.notificationToken ? this.notificationToken : null,
    });

    global.name = this.state.username;
    global.email = this.state.email;
    global.uid = uid;
    global.id = uid;
    global.role = this.state.role;
    console.log("user Data:", snapshot.val());
  };

  checkRole = (value) => {
    console.log("value =>>>", value);
    // this.setState({
    //   check:!this.state.check, role: 'admin'
    // });

    this.setState((prevState) => ({
      check: !prevState.check,
      role: !prevState.check ? "admin" : "user",
    }));
  };

  render() {
    console.log("this.state.role====>>>", this.state.role);
    const { userStore } = this.props.store;
    return (
      <View style={{ flex: 1 }}>
        <StatusBar backgroundColor="#004e00" barStyle="light-content" />
        <LinearGradient
          colors={["#004e00", "#00c004", "#001900"]}
          style={st.centerView}
        >
          <View style={{ marginTop: 10 }} />
          <TouchableOpacity onPress={this.handleUserIcon}>
            <Thumbnail
              source={
                this.state.imgloader
                  ? loader
                  : this.state.photoUrl
                  ? { uri: this.state.photoUrl }
                  : images.icProfilePlaceholder
              }
              style={{
                borderRadius: 80,
                justifyContent: "center",
                alignItems: "center",
                width: 150,
                height: 150,
              }}
            />
          </TouchableOpacity>
          <View style={st.contentFormScroll}>
            {/* <View style={st.separator} /> */}

            <ScrollView>
              <ArchInput
                name="username"
                onchangetext={(text) => {
                  this.setState({ username: text, error: "" });
                }}
              />
              <ArchInput
                name="email"
                onchangetext={(text) =>
                  this.setState({ email: text, error: "" })
                }
              />
              <ArchInput
                name="location"
                onchangetext={(text) =>
                  this.setState({ location: text, error: "" })
                }
              />
              <ArchInput
                name="address"
                onchangetext={(text) =>
                  this.setState({ address: text, error: "" })
                }
              />
              <ArchInput
                name="mobile"
                onchangetext={(text) =>
                  this.setState({ mobile: text, error: "" })
                }
              />
              <ArchInput
                name="password"
                onchangetext={(text) =>
                  this.setState({ password: text, error: "" })
                }
              />
              <ArchInput
                name="newPassword"
                onchangetext={(text) =>
                  this.setState({ newPassword: text, error: "" })
                }
              />

              <ArchRoundedButton
                text="REGISTER"
                navigation={this.props.navigation}
                onpress={() => {
                  if (this.state.email && this.state.password) {
                    auth()
                      .createUserWithEmailAndPassword(
                        this.state.email,
                        this.state.password
                      )
                      .then(() => {
                        this.storeUserInfo();
                        this.props.navigation.navigate("MainApp");
                      })
                      .catch((error) => {
                        let err = error + "sdf";
                        err = err.split("] ")[1];
                        this.setState({ error: err });
                      });
                  } else {
                    this.setState({
                      error: "Kindly provide email address and password",
                    });
                  }
                }}
              />
            </ScrollView>
          </View>
          <Fragment>
            {this.state.error ? (
              <Text style={{ color: "white", paddingTop: 15 }}>
                {this.state.error}
              </Text>
            ) : null}
            <View style={{ flexDirection: "row" }}>
              <CheckBox
                style={{ marginLeft: 0 }}
                value={this.state.check}
                onValueChange={this.checkRole}
              />
              <Text style={{ color: "white", marginTop: 4 }}>
                Register As A Maulana
              </Text>
            </View>

            <View style={{ flexDirection: "row" }}>
              <Button transparent>
                <Text style={{ color: "white" }}>Have an account?</Text>
              </Button>
              <Button
                transparent
                style={{ marginLeft: 5 }}
                onPress={() => this.props.navigation.navigate("Login")}
              >
                <Text style={{ color: "white", fontWeight: "bold" }}>
                  Login Here
                </Text>
              </Button>
            </View>
          </Fragment>
        </LinearGradient>
      </View>
    );
  }
}

export default inject("store")(observer(RegisterScreen));
