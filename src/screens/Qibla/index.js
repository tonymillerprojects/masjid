import React, { Component } from 'react'
import {
  View,
  PermissionsAndroid,
  Alert,
  ActivityIndicator,
  Dimensions,
  Platform,
  Text
} from 'react-native'
import colors from './../../assets/colors'
import st from './../../assets/styles'
import moment from 'moment'
import Geolocation from '@react-native-community/geolocation'
import Geocoder from 'react-native-geocoding'
import { WebView } from 'react-native-webview';

import {
  Container,
  Content,
  List,
  ListItem,
  Left,
  Right,
  Body,
  Button
} from 'native-base'

import * as locationService from './../../services/location'
import ArchHeader from './../../components/ArchHeader'
import { observer, inject } from 'mobx-react'

const { height } = Dimensions.get('window');

class Qibla extends Component {
  constructor(props) {
    super(props)
    this.state = {
      permissionsGranted: false,
    }
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      locationService.requestLocationPermission(isGranted => {
        if (isGranted) {
          this.requestCameraAndAudioPermission();
        } else {
          console.log('location permission denied')
        }
      });
    } else {
      this.setState({ permissionsGranted: true });
    }
  }

  requestCameraAndAudioPermission = async () => {
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.CAMERA,
        // PermissionsAndroid.PERMISSIONS.RECORD_AUDIO
      ]);

      if (granted['android.permission.CAMERA'] === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the camera");
        this.setState({ permissionsGranted: true });
      } else {
        console.log("Camera permission denied");
      }
    } catch (err) {
      console.log('request camera permission error: ', err);
    }
  }

  renderLoading = () => (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <ActivityIndicator
        color={colors.primaryColor}
        size={'large'}
      />
    </View>
  );

  render() {
    const { getTranslatedString } = this.props.store.strings;
    return (
      <Container>
        <ArchHeader {...this.props} title={getTranslatedString("QIBLA LOCATER")} isLoading={this.state.isLoading} />
        <Content style={{ backgroundColor: colors.backgroundColor, flex: 1 }}>
          {this.state.permissionsGranted ?
            <WebView
              source={{ uri: 'https://qiblafinder.withgoogle.com/intl/en/desktop/finder' }}
              style={{ height: height - 60 }}
              renderLoading={this.renderLoading}
            />
            :
            <View
              style={{
                marginHorizontal: 15,
                alignItems: 'center',
                justifyContent: 'center',
                height: height - 60
              }}
            >
              <Text
                style={{
                  fontSize: 18
                }}
              >
                We need location and camera permission to show the QIBLA DIRECTION.
              </Text>
            </View>
          }
        </Content>
      </Container>
    );
  }
}

export default inject('store')(observer(Qibla));
