import React, { Component } from "react";
import { View, Text, Alert, FlatList, ScrollView } from "react-native";
import colors from "./../../assets/colors";
import st from "./../../assets/styles";
import moment from "moment";
import Geocoder from "react-native-geocoding";

import * as locationService from "./../../services/location";
import * as calendarService from "./../../services/calendar";

import {
  Container,
  Content,
  List,
  ListItem,
  Left,
  Header,
  Thumbnail,
  Body,
  Right,
  Button,
  Icon,
  Input,
  Item,
} from "native-base";

import ArchHeader from "./../../components/ArchHeader";
import ArchHero from "./../../components/ArchHero";
import { observer, inject } from "mobx-react";
import database from "@react-native-firebase/database";
import auth from "@react-native-firebase/auth";

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return parseFloat(d).toFixed(2);
}

function deg2rad(deg) {
  return deg * (Math.PI / 180);
}

class Prayer extends Component {
  constructor(props) {
    super(props);
    this.getUserDetail();
    this.getMasjid();
    this.state = {
      prayers: [],
      defaultMasjid: null,
      hasDefaultMasjid: null,
      masjid: [],
      lat: 0,
      long: 0,
    };
  }

  componentDidMount() {
    locationService.requestLocationPermission((isGranted) => {
      if (isGranted) {
        this.setState({ isLoading: true });
        locationService.getLocation((coords) => {
          if (coords != null) {
            this.setState({
              lat: parseFloat(coords.latitude),
              long: parseFloat(coords.longitude),
            });
            locationService
              .getNearbyMasjids(coords.latitude, coords.longitude)
              .then(async (e) => console.log(await a.json()));
            this.getCity(coords.latitude, coords.longitude);
            this.getPrayerTimes(coords.latitude, coords.longitude);
          }
        });
      }
    });
  }

  getUserDetail = () => {
    const uid = auth().currentUser.uid;
    const ref = database().ref(`/users/${uid}`);
    ref.on("value", (snapshot) => {
      const values = snapshot.val();
      this.getPrayers(values.defaultMasjid);
      this.setState({
        defaultMasjid: values.defaultMasjid,
        hasDefaultMasjid:
          values.defaultMasjid && values.defaultMasjidName ? true : false,
      });
    });
  };

  getMasjid = () => {
    database()
      .ref("masjid")
      .on("value", (snapshot) => {
        const values = snapshot.val();
        const masjid = [];
        Object.keys(values).map((e) => {
          values[e].id = e;
          masjid.push(values[e]);
          console.log(values[e]);
        });
        this.setState({ masjid });
      });
  };

  setDefaultMasjid = (masjidId, masjidName) => {
    const uid = auth().currentUser.uid;
    const ref = database().ref(`/users/${uid}`);
    ref.once("value", (e) => {
      const data = e.val();
      data.defaultMasjid = masjidId;
      data.defaultMasjidName = masjidName;
      e.ref.set(data);
      this.setState({ defaultMasjid: masjidId, hasDefaultMasjid: true });
      this.getPrayers(masjidId);
    });
    // set({ defaultMasjid: true }, { merge: true });
  };

  getPrayers = (masjidId) => {
    masjidId &&
      database()
        .ref("prayers")
        .orderByChild("defaultMasjid")
        .equalTo(masjidId)
        .on("value", async (snapshot) => {
          if (snapshot._snapshot.exists) {
            const values = await snapshot.val();
            const prayers = [];
            Object.keys(values).map((e) => {
              values[e].id = e;
              prayers.push(values[e]);
              console.log("prayers", values[e]);
            });
            this.setState({ prayers });
          }
        });
  };

  getCity = (lat, long) => {
    Geocoder.from(lat, long)
      .then((res) => {
        let addressComponent = res.results[0].address_components[4];
        this.setState({
          cityName: addressComponent.long_name,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  getPrayerTimes = (lat, long) => {
    calendarService
      .getPrayerTimes(null, lat, long)
      .then((res) => {
        const response = res.data;
        if (response.code === 200) {
          this.setState({
            timings: response.data.timings,
            isLoading: false,
          });
        } else {
          Alert.alert("Error", response.status);
        }

        this.setState({ isLoading: false });
      })
      .catch((error) => {
        if (!error.status) {
          Alert.alert("Error", "Network Error");
        }

        this.setState({ isLoading: false });
      });
  };

  handleSearch = (searchText) => {
    var input, filter, li, a, i, txtValue;
    input = searchText;
    filter = input.toUpperCase();
    li = this.state.masjid;
    for (i = 0; i < li.length; i++) {
      a = li[i].name;
      txtValue = a;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        li[i].hide = false;
      } else {
        li[i].hide = true;
      }
    }
    this.setState({ masjid: li, searchText });
  };

  handleClear = () => {
    this.handleSearch("");
    this.setState({ searchText: "" });
  };

  onClickResetDefault = () =>
    this.setState({ hasDefaultMasjid: false, prayers: [] });

  render() {
    const { getTranslatedString } = this.props.store.strings;

    const defaultPrayer = [
      { name: "FAJR", azaanTime: null, jamaatTime: null },
      { name: "ZUHR", azaanTime: null, jamaatTime: null },
      { name: "ASR", azaanTime: null, jamaatTime: null },
      { name: "MAGHRIB", azaanTime: null, jamaatTime: null },
      { name: "ISHA", azaanTime: null, jamaatTime: null },
      { name: "JUMMNAH", azaanTime: null, jamaatTime: null },
    ];
    const singlePrayer = this.state.prayers ? this.state.prayers[0] : [];
    const firstPrayer = singlePrayer
      ? singlePrayer.Prayers && singlePrayer.Prayers.slice(0, 6)
      : defaultPrayer;
    const secondPrayer = singlePrayer ? singlePrayer.Prayers[7] : null;
    const thirdPrayer = singlePrayer ? singlePrayer.Prayers[9] : null;
    console.log("++++", singlePrayer, firstPrayer, "++++++");

    if (
      this.state.hasDefaultMasjid === null ||
      this.state.defaultMasjid === null
    ) {
      return (
        <Container>
          <ArchHeader {...this.props} />
          <View
            style={{
              flex: 10,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text>Loading...</Text>
          </View>
        </Container>
      );
    } else if (!this.state.hasDefaultMasjid) {
      return (
        <Container>
          <ArchHeader
            {...this.props}
            title={getTranslatedString("Set Masjid")}
          />
          <View style={{ flex: 10 }}>
            <View
              style={{
                paddingHorizontal: 30,
                paddingVertical: 20,
                justifyContent: "center",
                alignItems: "center",
                // borderBottomWidth: 2,
                // borderBottomColor: "#808080",
                // backgroundColor: "#6b2d",
              }}
            >
              <Text style={{ color: "#000", textAlign: "center" }}>
                {/* {getTranslatedString( */}
                In order for you to view the salah times that is closest to you.
                Please set your default masjid below
                {/* )} */}
              </Text>
            </View>
            <Header
              searchBar
              style={{ backgroundColor: "white" }}
              androidStatusBarColor="#006b2d"
            >
              <Item>
                <Icon name="ios-search" />
                <Input
                  onChangeText={this.handleSearch}
                  value={this.state.searchText}
                  placeholder="Search"
                />
                <Icon name="ios-close" onPress={this.handleClear} />
              </Item>
            </Header>
            <ScrollView>
              <Content>
                <List>
                  {this.state.masjid.map(
                    (ele) =>
                      !ele.hide && (
                        <ListItem thumbnail style={{ marginTop: 10 }}>
                          <Left>
                            <Thumbnail
                              style={{ borderRadius: 10 }}
                              square
                              large
                              source={{
                                uri: ele.imageUrl
                                  ? ele.imageUrl
                                  : "https://pngimage.net/wp-content/uploads/2018/06/logo-masjid-png-4.png",
                              }}
                            />
                          </Left>
                          <Body
                            style={{
                              marginLeft: 15,
                              padding: 0,
                            }}
                          >
                            <Text style={{ color: "black" }}>{ele.name}</Text>
                            <View
                              style={{
                                display: "flex",
                                flexDirection: "row",
                                alignItems: "center",
                              }}
                            >
                              <Icon
                                name="room"
                                type="MaterialIcons"
                                style={{ fontSize: 22 }}
                              />
                              <Text>
                                {getDistanceFromLatLonInKm(
                                  ele.lat,
                                  ele.long,
                                  this.state.lat,
                                  this.state.long
                                ) + " KM"}
                              </Text>
                            </View>
                          </Body>
                          <Right>
                            <Button
                              onPress={() =>
                                this.setDefaultMasjid(ele.id, ele.name)
                              }
                              style={{
                                borderRadius: 4,
                                color: "white",
                                backgroundColor: "#006b2d",
                                paddingHorizontal: 15,
                              }}
                            >
                              <Text style={{ color: "white", fontSize: 12 }}>
                                Set Default
                              </Text>
                            </Button>
                          </Right>
                        </ListItem>
                      )
                  )}
                </List>
              </Content>
            </ScrollView>
          </View>
        </Container>
      );
    } else {
      return (
        <Container>
          <ArchHeader
            {...this.props}
            title={getTranslatedString("SALAAH TIMES")}
            onClickResetDefault={this.onClickResetDefault}
          />
          <Content style={{ backgroundColor: "#d4d4d4" }}>
            <Content>
              <View
                style={{
                  flex: 1,
                  height: 50,
                  paddingLeft: 10,
                  justifyContent: "center",
                  alignItems: "center",
                  borderBottomWidth: 2,
                  borderBottomColor: "#808080",
                  backgroundColor: "#d4d4d4",
                }}
              >
                <Text style={{ color: "#006b2d", fontWeight: "bold" }}>
                  MASJID SALAAH TIMES
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  height: 390,
                  // borderBottomWidth: 3,
                  padding: 10,
                  alignContent: "space-between",
                  // borderBottomColor: "#808080",
                  backgroundColor: "#d4d4d4",
                }}
              >
                <FlatList
                  data={[
                    {
                      name: "PRAYERS",
                      azaanTime: "AZAAN",
                      jamaatTime: "IQAAMAH",
                    },
                    ...firstPrayer,
                  ]}
                  renderItem={({ item }) => (
                    <View
                      style={{
                        flexDirection: "row",
                        height: 50,
                        alignItems: "center",
                        justifyContent: "space-between",
                        borderRadius: 5,
                        backgroundColor: item.name === "PRAYERS" ? "" : "#fff",
                        paddingVertical: 5,
                        paddingHorizontal: 15,
                      }}
                    >
                      <Text
                        style={{ flex: 1, color: "#000", fontWeight: "bold" }}
                      >
                        {item.name}
                      </Text>
                      <Text style={{ color: "#000", fontWeight: "bold" }}>
                        {item.azaanTime}
                      </Text>
                      <Text
                        style={{
                          flex: 1,
                          color: "#000",
                          fontWeight: "bold",
                          textAlign: "right",
                        }}
                      >
                        {item.jamaatTime}
                      </Text>
                    </View>
                  )}
                  ItemSeparatorComponent={() => (
                    <View style={{ height: 3, backgroundColor: "808080" }} />
                  )}
                />
              </View>
              {/* sadfasdfasdfadfasdf */}
              <View
                style={{
                  flex: 1,
                  height: 50,
                  paddingLeft: 10,
                  justifyContent: "center",
                  alignItems: "center",
                  borderBottomWidth: 2,
                  borderBottomColor: "#808080",
                  backgroundColor: "#d4d4d4",
                }}
              >
                <Text style={{ color: "#006b2d", fontWeight: "bold" }}>
                  {" "}
                  Sunday's & Public Holiday's
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  height: 70,
                  borderBottomWidth: 2,
                  padding: 10,
                  alignContent: "space-between",
                  borderBottomColor: "#808080",
                  backgroundColor: "#d4d4d4",
                }}
              >
                <FlatList
                  data={[
                    secondPrayer
                      ? secondPrayer
                      : { name: "ZUHR", azaanTime: null, jamaatTime: null },
                  ]}
                  renderItem={({ item }) => (
                    <View
                      style={{
                        flexDirection: "row",
                        height: 45,
                        alignItems: "center",
                        justifyContent: "space-between",
                        borderRadius: 5,
                        backgroundColor: "#fff",
                        paddingVertical: 5,
                        paddingHorizontal: 15,
                      }}
                    >
                      <Text
                        style={{ flex: 1, color: "#000", fontWeight: "bold" }}
                      >
                        {item.name}
                      </Text>
                      <Text style={{ color: "#000", fontWeight: "bold" }}>
                        {item.azaanTime}
                      </Text>
                      <Text
                        style={{
                          flex: 1,
                          color: "#000",
                          fontWeight: "bold",
                          textAlign: "right",
                        }}
                      >
                        {item.jamaatTime}
                      </Text>
                    </View>
                  )}
                  ItemSeparatorComponent={() => (
                    <View style={{ height: 2, backgroundColor: "808080" }} />
                  )}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  height: 50,
                  paddingLeft: 10,
                  justifyContent: "center",
                  alignItems: "center",
                  borderBottomWidth: 2,
                  borderBottomColor: "#808080",
                  backgroundColor: "#d4d4d4",
                }}
              >
                <Text style={{ color: "#006b2d", fontWeight: "bold" }}>
                  Next Salaah
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  height: 70,
                  borderBottomWidth: 2,
                  padding: 10,
                  alignContent: "space-between",
                  borderBottomColor: "#808080",
                  backgroundColor: "#d4d4d4",
                }}
              >
                <FlatList
                  data={[
                    thirdPrayer
                      ? thirdPrayer
                      : { name: "ISHA", azaanTime: null, jamaatTime: null },
                  ]}
                  renderItem={({ item }) => (
                    <View
                      style={{
                        flexDirection: "row",
                        height: 45,
                        alignItems: "center",
                        justifyContent: "space-between",
                        borderRadius: 5,
                        backgroundColor: "#fff",
                        paddingVertical: 5,
                        paddingHorizontal: 15,
                      }}
                    >
                      <Text
                        style={{ flex: 1, color: "#000", fontWeight: "bold" }}
                      >
                        {item.name}
                      </Text>
                      <Text style={{ color: "#000", fontWeight: "bold" }}>
                        {item.azaanTime}
                      </Text>
                      <Text
                        style={{
                          flex: 1,
                          color: "#000",
                          fontWeight: "bold",
                          textAlign: "right",
                        }}
                      >
                        {item.jamaatTime}
                      </Text>
                    </View>
                  )}
                  ItemSeparatorComponent={() => (
                    <View style={{ height: 2, backgroundColor: "808080" }} />
                  )}
                />
              </View>
            </Content>
          </Content>
        </Container>
      );
    }
  }
}

export default inject("store")(observer(Prayer));
