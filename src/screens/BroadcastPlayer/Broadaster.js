import React, { Component, PureComponent } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Modal,
  NativeModules,
  Image,
  ActivityIndicator,
} from "react-native";
import uuid from "uuid";
import { RtcEngine, AgoraView } from "react-native-agora";
import { Surface } from "react-native-paper";
import database from "@react-native-firebase/database";
import { Alert } from "react-native";
import { appId, clientCertificate, clientId } from "../../utils/constants";
import Axios from "axios";
import { observer, inject } from "mobx-react";
const { Agora } = NativeModules;

if (!Agora) {
  throw new Error(
    "Agora load failed in react-native, please check ur compiler environments"
  );
}

const {
  FPS30,
  AudioProfileDefault,
  AudioScenarioDefault,
  Host,
  Adaptative,
  Audience,
} = Agora;

const BtnEndCall = () => require("../../assets/images/btn_endcall.png");
const BtnMute = () => require("../../assets/images/btn_mute.png");
const BtnSwitchCamera = () =>
  require("../../assets/images/btn_switch_camera.png");
const IconMuted = () => require("../../assets/images/icon_muted.png");

export const typeVideo = "Video";
export const typeAudio = "Audio";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F4F4F4",
  },
  absView: {
    position: "absolute",
    left: 15,
    right: 15,
    top: 15,
    height: 100,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  videoView: {
    padding: 5,
    flexWrap: "wrap",
    flexDirection: "row",
    zIndex: 100,
  },
  localView: {
    flex: 1,
  },
  remoteView: {
    width: (width - 40) / 3,
    height: (width - 40) / 3,
    margin: 5,
  },
  bottomView: {
    padding: 20,
    flexDirection: "row",
    justifyContent: "space-around",
  },
});

class OperateButton extends PureComponent {
  render() {
    const {
      onPress,
      source,
      style,
      imgStyle = { width: 50, height: 50 },
    } = this.props;
    return (
      <TouchableOpacity style={style} onPress={onPress} activeOpacity={0.7}>
        <Image style={imgStyle} source={source} />
      </TouchableOpacity>
    );
  }
}

type Props = {
  channelProfile: Number,
  channelName: String,
  clientRole: Number,
  type: String,
  onCancel: Function,
  uid: Number,
};

class AgoraRTCView extends Component<Props> {
  state = {
    peerIds: [],
    joinSucceed: false,
    isMute: false,
    hideButton: false,
    visible: false,
    selectedUid: undefined,
    animating: true,
    onlineUsers: 0,
    time: 0,
    channelId: this.props.channelId || this.props.channelName + "_" + uuid(),
    loading: true,
    resourseId: null,
    sId: null,
  };

  componentDidMount() {
    /* if (this.props.clientRole == Host) {
			this.setupAgoraEnvironment();
		} else {
			this.initializeAgora();
		} */

    this.initializeAgora();
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.navigation.isFocused();
  }

  setupAgoraEnvironment = async () => {
    try {
      const { uid, channelName } = this.props;

      const aquireChannelResponse = await Axios({
        url: `https://api.agora.io/v1/apps/${appId}/cloud_recording/acquire`,
        method: "POST",
        headers: {
          Authorization: `Basic ${btoa(`${clientId}:${clientCertificate}`)}`,
        },
        data: {
          cname: channelName,
          uid: String(uid),
          clientRequest: {
            // resourceExpiredHour: 24
          },
        },
      });

      console.log("aquireChannelResponse:", aquireChannelResponse.data);

      const resourseId = aquireChannelResponse.data.resourceId;

      const startRecordingResponse = await Axios({
        url: `https://api.agora.io/v1/apps/${appId}/cloud_recording/resourceid/${resourseId}/mode/mix/start`,
        method: "POST",
        headers: {
          Authorization: `Basic ${btoa(`${clientId}:${clientCertificate}`)}`,
        },
        data: {
          cname: channelName,
          uid: String(uid),
          clientRequest: {
            recordingConfig: {
              maxIdleTime: 30,
              streamTypes: 2,
              audioProfile: 1,
              channelType: 1,
              videoStreamType: 1,
              transcodingConfig: {
                height: 640,
                width: 360,
                bitrate: 800,
                fps: 15,
                mixedVideoLayout: 1,
                maxResolutionUid: "1",
              },
              subscribeVideoUids: ["123", "456"],
              subscribeAudioUids: ["123", "456"],
              subscribeUidGroup: 0,
            },
            storageConfig: {
              accessKey: "AKIAI5ZDKNCU32KP7BDA",
              region: 0,
              bucket: "masjidspeaker",
              secretKey: "qYg9jfxswOew1LaoDF7vfriQeu+OcrUyVeiq9aIY",
              vendor: 1,
              fileNamePrefix: ["archives"],
            },
          },
        },
      });

      console.log("startRecordingResponse: ", startRecordingResponse.data);

      this.setState({ resourseId, sId: startRecordingResponse.data.sid });

      this.initializeAgora();
    } catch (error) {
      console.log("aquire channel error: ", JSON.stringify(error, null, 2));
      const { getTranslatedString } = this.props.store.strings;
      Alert.alert(
        getTranslatedString("Error!"),
        getTranslatedString(
          "Cannot start broadcating now. Please try again later."
        ),
        [
          {
            text: getTranslatedString("Ok"),
            onPress: () => this.props.navigation.goBack(),
          },
        ]
      );
    }
  };

  initializeAgora = async () => {
    const config = {
      appid: appId,
      channelProfile: this.props.channelProfile,
      clientRole: this.props.clientRole,
      videoEncoderConfig: {
        width: 360,
        height: 480,
        bitrate: 1,
        frameRate: FPS30,
        orientationMode: Adaptative,
      },
      audioProfile: AudioProfileDefault,
      audioScenario: AudioScenarioDefault,
    };

    // console.log("[CONFIG]", JSON.stringify(config));
    // console.log("[CONFIG.encoderConfig", config.videoEncoderConfig);

    RtcEngine.on("videoSizeChanged", (data) => {
      console.log("[RtcEngine] videoSizeChanged ", data);
    });

    RtcEngine.on("remoteVideoStateChanged", (data) => {
      console.log("[RtcEngine] `remoteVideoStateChanged`", data);
    });

    RtcEngine.on("userJoined", (data) => {
      // console.log("[RtcEngine] onUserJoined", data);
      const { peerIds } = this.state;
      if (peerIds.indexOf(data.uid) === -1) {
        this.setState({ peerIds: [...peerIds, data.uid] }, () => {
          if (this.props.clientRole === Audience) {
            // this.updateData();

            database()
              .ref("broadcast/" + this.state.channelId)
              .update({
                peers: this.state.peerIds.length,
              });
          }
        });
      }
    });

    RtcEngine.on("userOffline", (data) => {
      console.log("[RtcEngine] onUserOffline", data);

      if (this.state.peerIds[0] === data.uid) {
        this.props.navigation.goBack();
      }
      this.setState(
        {
          peerIds: this.state.peerIds.filter((uid) => uid !== data.uid),
        },
        () => {}
      );
      console.log("peerIds", this.state.peerIds, "data.uid ", data.uid);
    });

    RtcEngine.on("joinChannelSuccess", (data) => {
      console.log("[RtcEngine] onJoinChannelSuccess", data);

      RtcEngine.startPreview()
        .then((_) => {
          if (this.props.clientRole === Host) {
            this.updateData();

            database()
              .ref("broadcast/" + this.state.channelId)
              .update({
                startTime: new Date(),
                channelId: this.state.channelId,
                uid: this.props.uuid,
                userName: global.name || global.email,
                userId: global.id,
              });
          }
          this.setState({
            joinSucceed: true,
            animating: false,
          });

          this.timer = setInterval(() => {
            this.setState({
              time: this.state.time + 1,
            });
          }, 1000);
        })
        .catch((error) => {
          console.log("start preview error: ", error);
        });
    });

    RtcEngine.on("audioVolumeIndication", (data) => {
      // console.log("[RtcEngine] onAudioVolumeIndication", data);
    });

    RtcEngine.on("clientRoleChanged", (data) => {
      // console.log("[RtcEngine] onClientRoleChanged", data);
    });

    RtcEngine.on("videoSizeChanged", (data) => {
      // console.log("[RtcEngine] videoSizeChanged", data);
    });

    RtcEngine.on("error", (data) => {
      console.log("[RtcEngine] onError", data);

      if (data.error === 17) {
        RtcEngine.leaveChannel().then((_) => {
          this.setState({
            joinSucceed: false,
          });
          const { state, goBack } = this.props.navigation;
          //this.props.onCancel(data);
          goBack();
        });
      }
    });

    RtcEngine.init(config);

    if (this.props.type === typeAudio) {
      RtcEngine.disableVideo()
        .then((res) => {
          console.log("[RtcEngine] disableVideo ", res);
        })
        .catch((err) => {
          console.log("[RtcEngine] disableVideo ", err);
        });
    }

    database()
      .ref("broadcast/" + this.state.channelId)
      .on("value", (snapshot) => {
        console.log("Firebase snapshot value: ", snapshot.val());

        const value = snapshot.val();
        if (value !== null) {
          this.updateState(value);
        }
      });

    RtcEngine.getSdkVersion((version) => {
      console.log("[RtcEngine] getSdkVersion", version);
    });

    RtcEngine.joinChannel(this.state.channelId, this.props.uid)
      .then((result) => {
        /**
         * ADD the code snippet after join channel success.
         */
      })
      .catch((error) => {
        console.log("join channel error: ", JSON.stringify(error));
        const { getTranslatedString } = this.props.store.strings;

        Alert.alert(
          getTranslatedString("Error!"),
          getTranslatedString(
            "Cannot start broadcating now. Please try again later."
          ),
          [
            {
              text: getTranslatedString("Ok"),
              onPress: () => this.props.navigation.goBack(),
            },
          ]
        );
      });

    RtcEngine.enableAudioVolumeIndication(500, 3, true);

    /* if (this.props.clientRole == Host) {
			setTimeout(async () => {
				try {
					const queryResponse = await fetch(`https://api.agora.io/v1/apps/${appId}/cloud_recording/resourceid/${this.state.resourseId}/sid/${this.state.sId}/mode/mix/query`, {
						headers: {
							Authorization: `Basic ${btoa(`${clientId}:${clientCertificate}`)}`,
							'Content-Type': 'application/json'
						},
					}).then((res) => res.json());

					console.log('query response: ', queryResponse);
				} catch (error) {
					// console.log('query error: ', JSON.stringify(error, null, 2));
					console.log('query error: ', error);
				}

			}, 4000);


			setTimeout(async () => {
				try {
					const queryResponse = await fetch(`https://api.agora.io/v1/apps/${appId}/cloud_recording/resourceid/${this.state.resourseId}/sid/${this.state.sId}/mode/mix/query`, {
						headers: {
							Authorization: `Basic ${btoa(`${clientId}:${clientCertificate}`)}`,
							'Content-Type': 'application/json'
						},
					}).then((res) => res.json());

					console.log('query response: ', queryResponse);
				} catch (error) {
					// console.log('query error: ', JSON.stringify(error, null, 2));
					console.log('query error: ', error);
				}

			}, 8000);
		} */
  };

  timer = null;

  updateData() {
    database()
      .ref("broadcast/" + this.state.channelId)
      .set({
        name: this.props.channelName,
        type: this.props.type,
        peers: this.state.peerIds.length,
        isEnd: false,
        userId: global.id,
      });
  }

  updateState = (data) => {
    this.setState({ onlineUsers: data.peers });
  };

  timeConversion(duration) {
    var milliseconds = parseInt((duration % 1000) / 100),
      seconds = Math.floor((duration / 1000) % 60),
      minutes = Math.floor((duration / (1000 * 60)) % 60),
      hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds;
  }

  componentWillUnmount() {
    if (this.state.joinSucceed) {
      RtcEngine.leaveChannel()
        .then((res) => {
          RtcEngine.destroy();
          if (this.props.clientRole === Host) {
            database()
              .ref("broadcast/" + this.state.channelId)
              .remove();
          } else {
            database()
              .ref("broadcast/" + this.state.channelId)
              .update({ peers: this.state.onlineUsers - 1 });
          }
        })
        .catch((err) => {
          RtcEngine.destroy();
          console.log("leave channel failed", err);
        });
    } else {
      RtcEngine.destroy();
    }

    clearInterval(this.timer);
  }

  handleCancel = async () => {
    const { goBack } = this.props.navigation;

    try {
      /* const stopResponse = await fetch(`https://api.agora.io/v1/apps/${appId}/cloud_recording/resourceid/${this.state.resourseId}/sid/${this.state.sId}/mode/mix/stop`, {
				method: 'POST',
				headers: {
					Authorization: `Basic ${btoa(`${clientId}:${clientCertificate}`)}`,
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					cname: this.props.channelName,
					uid: String(this.props.uid),
					clientRequest: {}
				})
			}).then((res) => res.json());

			console.log('stopResponse: ', stopResponse); */

      RtcEngine.leaveChannel()
        .then((_) => {
          this.setState({
            joinSucceed: false,
          });
          goBack();
          if (this.props.clientRole === Host) {
            database()
              .ref("broadcast/" + this.state.channelId)
              .update({
                isEnd: true,
                endTime: new Date(),
              });
          } else {
            database()
              .ref("broadcast/" + this.state.channelId)
              .update({ peers: this.state.onlineUsers - 1 });
          }
        })
        .catch((err) => {
          console.log("[agora]: err", err);
        });
    } catch (error) {
      console.log("stop recording error: ", JSON.stringify(error, null, 2));
      const { goBack } = this.props.navigation;
    }
  };

  switchCamera = () => {
    RtcEngine.switchCamera();
  };

  toggleAllRemoteAudioStreams = () => {
    this.setState(
      {
        isMute: !this.state.isMute,
      },
      () => {
        RtcEngine.muteAllRemoteAudioStreams(this.state.isMute).then((_) => {
          /**
           * ADD the code snippet after muteAllRemoteAudioStreams success.
           */
        });
      }
    );
  };

  toggleHideButtons = () => {
    this.setState({
      hideButton: !this.state.hideButton,
    });
  };

  onPressVideo = (uid) => {
    this.setState(
      {
        selectedUid: uid,
      },
      () => {
        this.setState({
          visible: true,
        });
      }
    );
  };

  toolBar = ({ hideButton, isMute }) => {
    if (!hideButton) {
      return (
        <View>
          <View style={styles.bottomView}>
            <OperateButton
              onPress={this.toggleAllRemoteAudioStreams}
              source={isMute ? IconMuted() : BtnMute()}
            />
            <OperateButton
              style={{ alignSelf: "center", marginBottom: -10 }}
              onPress={this.handleCancel}
              imgStyle={{ width: 60, height: 60 }}
              source={BtnEndCall()}
            />
            {this.props.type === typeVideo ? (
              <OperateButton
                onPress={this.switchCamera}
                source={BtnSwitchCamera()}
              />
            ) : null}
          </View>
        </View>
      );
    }
  };

  agoraPeerViews = ({ visible, peerIds }) => {
    return visible ? (
      <View style={styles.videoView} />
    ) : (
      <View style={styles.videoView}>
        {peerIds.map((uid, key) => (
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => this.onPressVideo(uid)}
            key={key}
          >
            <Text>uid: {uid}</Text>
            <AgoraView
              mode={1}
              key={uid}
              style={styles.remoteView}
              zOrderMediaOverlay={true}
              remoteUid={uid}
            />
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  selectedView = ({ visible }) => {
    return (
      <Modal
        visible={visible}
        presentationStyle={"fullScreen"}
        animationType={"slide"}
        onRequestClose={() => {}}
      >
        <TouchableOpacity
          activeOpacity={1}
          style={{ flex: 1 }}
          onPress={() =>
            this.setState({
              visible: false,
            })
          }
        >
          <AgoraView
            mode={1}
            style={{ flex: 1 }}
            zOrderMediaOverlay={true}
            remoteUid={this.state.selectedUid}
          />
        </TouchableOpacity>
      </Modal>
    );
  };

  render() {
    const { getTranslatedString } = this.props.store.strings;

    const isVideo = this.props.type === typeVideo;
    // console.log(this.state);
    if (!this.state.joinSucceed) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: "#fff",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <ActivityIndicator animating={this.state.animating} size={"large"} />
        </View>
      );
    }

    return (
      <View style={{ flex: 1 }}>
        {isVideo ? (
          this.props.clientRole === Audience ? (
            <AgoraView
              mode={1}
              key={this.state.peerIds[0]}
              style={styles.localView}
              remoteUid={this.state.peerIds[0]}
            />
          ) : (
            <Surface
              activeOpacity={1}
              onPress={this.toggleHideButtons}
              style={styles.container}
            >
              <AgoraView style={styles.localView} showLocalVideo mode={1} />
              <View
                style={{
                  position: "absolute",
                  bottom: 0,
                  height: 100,
                  left: 0,
                  right: 0,
                }}
              >
                {this.toolBar(this.state)}
              </View>
            </Surface>
          )
        ) : this.props.clientRole === Audience ? (
          <Surface
            activeOpacity={1}
            onPress={this.toggleHideButtons}
            style={styles.container}
          >
            <View
              style={{
                position: "absolute",
                bottom: 0,
                left: 0,
                right: 0,
                top: 0,
                backgroundColor: "black",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text style={{ color: "white", fontSize: 36 }}>
                {this.timeConversion(this.state.time * 1000)}
              </Text>
            </View>
            <AgoraView
              style={styles.localView}
              showLocalVideo={false}
              mode={1}
            />
          </Surface>
        ) : (
          <Surface
            activeOpacity={1}
            onPress={this.toggleHideButtons}
            style={styles.container}
          >
            <View
              style={{
                position: "absolute",
                bottom: 0,
                left: 0,
                right: 0,
                top: 0,
                backgroundColor: "black",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text style={{ color: "white", fontSize: 36 }}>
                {this.timeConversion(this.state.time * 1000)}
              </Text>
            </View>
            <AgoraView
              style={styles.localView}
              showLocalVideo={false}
              mode={1}
            />
            <View
              style={{
                position: "absolute",
                bottom: 0,
                height: 100,
                left: 0,
                right: 0,
              }}
            >
              {this.toolBar(this.state)}
            </View>
          </Surface>
        )}
        <View style={styles.absView}>
          <Text style={{ color: "white", fontSize: 20 }}>
            {getTranslatedString("Name :")} {this.props.channelName}
          </Text>
          <Text style={{ color: "white", fontSize: 20 }}>
            {this.state.onlineUsers}
          </Text>
        </View>
      </View>
    );
  }
}

function Broadaster(props) {
  const { navigation } = props;
  const channelProfile = navigation.getParam("channelProfile", 1);
  const clientRole = navigation.getParam("clientRole", Host);
  const channelId = navigation.getParam("channelId", null);
  const channelName = navigation.getParam("channelName", "agoratest");
  const uid = navigation.getParam(
    "uid",
    Math.floor(Math.random() * 899999 + 100000)
  );
  const type = navigation.getParam("type", typeAudio);
  const onCancel = navigation.getParam("onCancel");

  return (
    <AgoraRTCView
      channelProfile={channelProfile}
      channelId={channelId}
      channelName={channelName}
      clientRole={clientRole}
      uid={uid}
      onCancel={onCancel}
      type={type}
      {...props}
    />
  );
}

export default inject("store")(observer(Broadaster));
