import React, { Component } from "react";
import { Alert, View, ActivityIndicator, StyleSheet } from "react-native";
import { observer, inject } from "mobx-react";
import moment from "moment";
import { Container, Content, Spinner } from "native-base";
import {
  PowerTranslator,
  ProviderTypes,
  TranslatorConfiguration,
  TranslatorFactory
} from "react-native-power-translator";
import { Col, Row, Grid } from "react-native-easy-grid";
import RNPickerSelect from "react-native-picker-select";
import auth, { firebase } from "@react-native-firebase/auth";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes
} from "@react-native-community/google-signin";
import { AccessToken, LoginManager, LoginButton } from "react-native-fbsdk";

import database from "@react-native-firebase/database";
import * as calendarService from "./../../services/calendar";

import colors from "./../../assets/colors";
import st from "./../../assets/styles";
import ArchHeader from "./../../components/ArchHeader";
import ArchHero from "./../../components/ArchHero";
import ArchMenuButton from "./../../components/ArchMenuButton";
import { PickerRightIcon } from "../../components/PickerRightIcon";
import { languages } from "../../utils/languages";
import { googleTranslateAPIKey } from "../../utils/constants";
import messaging from "@react-native-firebase/messaging";

async function registerAppWithFCM() {
  await messaging()
    .registerDeviceForRemoteMessages()
    .then(e => console.log("success", e))
    .catch(e => console.log("err", e));
}

class WelcomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      nowIs: moment().format("hh:mm A"),
      hijriDate: "",
      language: "en",
      translatingStrings: false
    };

    TranslatorConfiguration.setConfig(
      ProviderTypes.Google,
      googleTranslateAPIKey,
      "en"
    );
    registerAppWithFCM();
  }

  async componentDidMount() {
    this.getTodayTime();
    this.getTodayDate();

    if (
      !global.role ||
      !global.name ||
      !global.defaultMasjidName ||
      !global.photoUrl
    ) {
      const userData = await database()
        .ref(`/users/${global.id}`)
        .once("value");
      const userValue = userData.val();
      global.photoUrl = userValue.photoUrl;
      global.name = userValue.name;
      global.role = userValue.role;
      global.defaultMasjidName = userValue.defaultMasjidName;
    }
  }

  translateAppStrings = async language => {
    try {
      // console.log('selected language: ', language)
      this.setState({ language });
      this.setState({ translatingStrings: true });

      TranslatorConfiguration.setConfig(
        ProviderTypes.Google,
        googleTranslateAPIKey,
        language
      );

      const translator = TranslatorFactory.createTranslator();
      const allStrings = this.props.store.strings.toJSON().strings;

      const promises = allStrings.map(stringObj =>
        translator.translate(stringObj.original, language).then(translated => {
          return {
            original: stringObj.original,
            translated
          };
        })
      );

      const translatedStrings = await Promise.all(promises);
      // console.log('translatedStrings: ', translatedStrings)
      this.props.store.strings.setStrings(translatedStrings);

      this.setState({ translatingStrings: false });
    } catch (error) {
      console.log("error: ", error);
      Alert.alert(
        "Translation error!",
        "Something went wrong. Please try again later."
      );

      this.setState({ translatingStrings: false });
    }
  };

  getTodayDate = () => {
    this.setState({ isLoading: true });
    calendarService
      .getTodayDate()
      .then(res => {
        const response = res.data;
        const hijri = response.data.hijri;

        const day = hijri.day;
        const month = hijri.month.en;
        const year = hijri.year;
        const designation = hijri.designation.abbreviated;

        const hijriDate = day + " " + month + ", " + year + " " + designation;

        this.setState({
          hijriDate: hijriDate,
          isLoading: false
        });
      })
      .catch(error => {
        if (!error.status) {
          Alert.alert("Error", "Network Error");
        }

        this.setState({ isLoading: false });
      });
  };

  getTodayTime = () => {
    setInterval(() => {
      let date = moment().format("hh:mm A");
      this.setState({
        nowIs: date,
        userInfo: this.props.userInfo
      });
    }, 5000);
  };

  goToMenu = menu => {
    const { navigation } = this.props;
    navigation.navigate(menu);
  };

  signOut = async () => {
    global.name = "";
    global.id = "";
    global.email = "";
    global.photoUrl = "";
    const isSignedIn = await GoogleSignin.isSignedIn();
    if (isSignedIn) {
      try {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
        this.props.navigation.navigate("LoginStack");
      } catch (error) {
        console.error(error);
      }
    }

    try {
      await firebase.auth().signOut();
      this.props.navigation.navigate("LoginStack");
    } catch (e) {
      console.log(e);
    }
    LoginManager.logOut();
    this.props.navigation.navigate("LoginStack");
  };

  translateApp = async language => {
    try {
      const strings = this.props.store.strings.strings;
      console.log("all strings: ", strings);
      // const strings = this.
    } catch (error) {
      console.log("translate app error: ", error);
    }
  };

  renderLanguageButton = () => {
    return (
      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <RNPickerSelect
          onValueChange={this.translateAppStrings}
          value={this.state.language}
          items={languages}
          placeholder={{}}
          style={{
            inputAndroid: {
              fontSize: 14,
              color: "white",
              fontWeight: "bold",
              paddingRight: 5
            },
            iconContainer: {
              top: 14,
              right: -17
            }
          }}
          Icon={() => <PickerRightIcon />}
          useNativeAndroidPickerStyle={false}
        />
      </View>
    );
  };

  render() {
    const { getTranslatedString } = this.props.store.strings;

    return (
      <Container>
        <ArchHeader
          {...this.props}
          title={getTranslatedString("Home")}
          logout={true}
          isLoading={this.state.isLoading}
          onpress={() => this.signOut()}
          renderCenterRightButton={this.renderLanguageButton}
        />
        <Content
          style={{ backgroundColor: colors.backgroundColor }}
          contentContainerStyle={{ paddingBottom: 40 }}
        >
          <ArchHero
            for="welcome"
            currentTime={this.state.nowIs}
            currentDate={this.state.hijriDate}
          />
          <Content>
            {/* <PowerTranslator
              style={{
                fontWeight: 'bold',
                fontSize: 16
              }}
              text={'Hello'}
            /> */}
            <Grid>
              <Row style={st.menuRow}>
                <Col style={st.menuCol}>
                  <ArchMenuButton
                    icon="prayer"
                    onPress={menu => this.goToMenu("Prayer")}
                    label={getTranslatedString("SALAAH TIMES")}
                  />
                </Col>
                <Col style={st.menuCol}>
                  <ArchMenuButton
                    icon="broadcast"
                    onPress={menu => this.goToMenu("Broadcasting")}
                    label={getTranslatedString("AUDIO & VIDEO BROADCASTING")}
                  />
                </Col>
              </Row>
              <Row style={st.menuRow2}>
                <Col style={st.menuCol}>
                  <ArchMenuButton
                    icon="events"
                    onPress={menu => this.goToMenu("Event")}
                    label={getTranslatedString("MASJID EVENTS & ANNOUNCEMENTS")}
                  />
                </Col>

                <Col style={st.menuCol}>
                  <ArchMenuButton
                    icon="masjid"
                    onPress={menu => this.goToMenu("Masjid")}
                    label={getTranslatedString("MASJID LOCATION")}
                  />
                </Col>
                <Col style={st.menuCol}>
                  <ArchMenuButton
                    icon="qibla"
                    onPress={menu => this.goToMenu("Qibla")}
                    label={getTranslatedString("QIBLA LOCATER")}
                  />
                </Col>
              </Row>
            </Grid>
          </Content>
        </Content>
        {this.state.translatingStrings ? (
          <View
            style={{
              ...StyleSheet.absoluteFill,
              backgroundColor: colors.black1,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <ActivityIndicator color={colors.backgroundColor} size={"large"} />
          </View>
        ) : null}
      </Container>
    );
  }
}

export default inject("store")(observer(WelcomeScreen));
