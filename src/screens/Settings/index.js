import React, { Component } from "react";

import { observer, inject } from "mobx-react";
import {
  Container,
  Content,
  ListItem,
  Left,
  Icon,
  Body,
  Text,
  Right,
  Switch
} from "native-base";
import ArchHeader from "./../../components/ArchHeader";
import database from "@react-native-firebase/database";

class SettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { switch: false };
  }

  componentDidMount = async () => {
    await database()
      .ref(`/users/${global.id}`)
      .on("value", async e => {
        const data = await e.toJSON();
        this.setState({
          isPrayerNotification: data.isPrayerNotification,
          isEventNotification: data.isEventNotification,
          isBroadcastNotification: data.isBroadcastNotification
        });
      });
  };
  togglePermission = key => {
    database()
      .ref(`/users/${global.id}`)
      .update({ [key]: !this.state[key] });
  };
  render() {
    const { getTranslatedString } = this.props.store.strings;
    const {
      isPrayerNotification,
      isEventNotification,
      isBroadcastNotification
    } = this.state;
    return (
      <Container>
        <ArchHeader
          {...this.props}
          title={getTranslatedString("Settings")}
          logout={true}
          isLoading={this.state.isLoading}
          onpress={() => this.signOut()}
          renderCenterRightButton={this.renderLanguageButton}
        />
        <Content>
          <ListItem icon>
            <Left>
              <Icon active name="alert" />
            </Left>
            <Body>
              <Text>Prayer notification</Text>
            </Body>
            <Right>
              <Switch
                value={isPrayerNotification}
                onResponderStart={() =>
                  this.togglePermission("isPrayerNotification")
                }
              />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Icon active name="alert" />
            </Left>
            <Body>
              <Text>Broadcast notification</Text>
            </Body>
            <Right>
              <Switch
                value={isEventNotification}
                onResponderStart={() =>
                  this.togglePermission("isEventNotification")
                }
              />
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Icon active name="alert" />
            </Left>
            <Body>
              <Text>Event notification</Text>
            </Body>
            <Right>
              <Switch
                value={isBroadcastNotification}
                onResponderStart={() =>
                  this.togglePermission("isBroadcastNotification")
                }
              />
            </Right>
          </ListItem>
        </Content>
      </Container>
    );
  }
}

export default inject("store")(observer(SettingsScreen));
