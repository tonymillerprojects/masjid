import React from "react";
import {
  View,
  Text,
  ImageBackground,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView
} from "react-native";
import { images } from "../../assets/images";
const { width, height } = Dimensions.get("window");
import store from "../../mst/stores";

import auth, { firebase } from "@react-native-firebase/auth";
import { GoogleSignin } from "@react-native-community/google-signin";
import { LoginManager } from "react-native-fbsdk";
import { observer, inject } from "mobx-react";
import { Thumbnail, Icon } from "native-base";
import Modal from "react-native-modal";
import storage from "@react-native-firebase/storage";
import ImagePicker from "react-native-image-picker";
import database from "@react-native-firebase/database";

class DrawerComponent extends React.Component {
  handleUserIcon = () => {
    const options = {
      title: "Select Image",
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        this.uploadUserIcon(response.uri);
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
      }
    });
  };
  uploadUserIcon = async filePath => {
    this.setState({ imgloader: true });
    const reference = storage().ref(`/user/${new Date().toJSON()}.jpeg`);
    await reference
      .putFile(filePath)
      .then(async e => {
        const url = await reference.getDownloadURL();
        global.photoUrl = url;
        this.setState({ imgloader: false });
        database()
          .ref(`/users/${global.id}`)
          .update({ photoUrl: url });
      })
      .catch(e => console.log("err", e));
  };
  render = () => {
    const props = this.props;
    const email = global.email;
    const name = global.name;
    const photo = global.photoUrl;
    const uid = global.uid;

    const { getTranslatedString } = this.props.store.strings;

    return (
      <View style={{ flex: 1, height: height - 30, padding: 0 }}>
        <ImageBackground
          source={images.back6}
          resizeMode="stretch"
          style={[styles.background]}
        >
          <TouchableOpacity onPress={this.handleUserIcon}>
            <Thumbnail
              style={{ height: 120, width: 120, borderRadius: 60 }}
              source={photo ? { uri: photo } : images.icProfilePlaceholder}
            />
          </TouchableOpacity>
          {name ? (
            <Text style={styles.nameText}>{name}</Text>
          ) : (
            <Text style={styles.nameText}>{email}</Text>
          )}
        </ImageBackground>
        <ScrollView style={{ marginTop: 20 }}>
          <TouchableOpacity
            style={styles.menuItem}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate("Welcome");
            }}
          >
            <Image source={images.home} style={styles.iconImg} />
            <Text style={styles.menuText}>{getTranslatedString("Home")}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItem}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate("Prayer");
            }}
          >
            <Image source={images.prayer} style={styles.iconImg} />
            <Text style={styles.menuText}>
              {getTranslatedString("Salaah Time")}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItem}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate("Broadcasting");
            }}
          >
            <Image
              source={images.broad}
              style={[styles.iconImg, { width: 20, height: 20 }]}
            />
            <Text style={styles.menuText}>
              {getTranslatedString("Audio & Video Broadcasting")}
            </Text>
          </TouchableOpacity>
          {/* <TouchableOpacity style={styles.menuItem} onPress={() => {
          props.navigation.closeDrawer();
          props.navigation.navigate('Prayer')
        }
        }>
          <Image source={images.prayer} style={styles.iconImg} />
          <Text style={styles.menuText}>{getTranslatedString('Prayer Schedule')}</Text>
        </TouchableOpacity> */}
          <TouchableOpacity
            style={styles.menuItem}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate("Event");
            }}
          >
            <Image source={images.events} style={styles.iconImg} />
            <Text style={styles.menuText}>
              {getTranslatedString("Masjid Events & Announcements")}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItem}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate("Masjid");
            }}
          >
            <Image source={images.masjid} style={styles.iconImg} />
            <Text style={styles.menuText}>
              {getTranslatedString("Masjid location")}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItem}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate("Qibla");
            }}
          >
            <Image source={images.qibla} style={styles.iconImg} />
            <Text style={styles.menuText}>
              {getTranslatedString("Qibla Locater")}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItem}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate("Subscription");
            }}
          >
            <Image source={images.sub} style={styles.iconImg} />
            <Text style={styles.menuText}>
              {getTranslatedString("Subscription")}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.menuItem}
            onPress={() => {
              props.navigation.closeDrawer();
              props.navigation.navigate("Settings");
            }}
          >
            <Icon
              type="MaterialIcons"
              name="settings"
              style={{ color: "#006b2d", fontSize: 20 }}
            />
            <Text style={styles.menuText}>
              {getTranslatedString("Settings")}
            </Text>
          </TouchableOpacity>
          <View style={{ flex: 1 }} />
          <TouchableOpacity
            style={styles.menuItem2}
            onPress={() => {
              signOut(props);
            }}
          >
            <Image source={images.logout} style={styles.iconImg} />
            <Text style={styles.menuText}>
              {getTranslatedString("Sign Out")}
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  };
}

const signOut = async props => {
  global.name = "";
  global.id = "";
  global.uid = "";
  global.email = "";
  global.photoUrl = "";
  const isSignedIn = await GoogleSignin.isSignedIn();
  if (isSignedIn) {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      props.navigation.navigate("LoginStack");
    } catch (error) {
      console.error(error);
    }
  }

  try {
    await firebase.auth().signOut();
    props.navigation.navigate("LoginStack");
  } catch (e) {
    console.log(e);
  }
  LoginManager.logOut();
  props.navigation.navigate("LoginStack");
};

const styles = StyleSheet.create({
  background: {
    alignItems: "center",
    width: width * 0.8,
    height: height / 3 + 20,
    marginTop: -2,
    paddingTop: 20
  },
  avatar: {
    borderRadius: 55,
    justifyContent: "center",
    alignItems: "center",
    width: 110,
    height: 110
  },
  nameText: {
    color: "#ffffff",
    fontFamily: "Aileron",
    fontSize: 20,
    fontWeight: "600"
  },
  titleText: {
    color: "#ffffff",
    fontFamily: "Aileron",
    fontSize: 20,
    fontWeight: "300",
    marginTop: 4
  },
  menuItem: {
    flexDirection: "row",
    paddingBottom: 20,
    paddingLeft: 30,
    alignItems: "center"
  },
  menuItem2: {
    flexDirection: "row",
    padding: 20,
    paddingLeft: 30,
    borderTopColor: "#dbdbdb",
    borderTopWidth: 1,
    alignItems: "center"
  },
  menuText: {
    width: 220,
    color: "#717171",
    fontFamily: "Aileron",
    fontSize: 16,
    fontWeight: "700",
    marginLeft: 20,
    textTransform: "capitalize"
  },
  iconImg: {
    width: 20,
    height: 20,
    resizeMode: "stretch"
  }
});

const Drawer = inject("store")(observer(DrawerComponent));

export { Drawer };
