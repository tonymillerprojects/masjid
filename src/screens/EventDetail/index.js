import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  PermissionsAndroid,
  Dimensions,
} from "react-native";
import colors from "./../../assets/colors";
import st from "./../../assets/styles";

import {
  Container,
  Content,
  Card,
  CardItem,
  Body,
  Icon,
  Thumbnail,
} from "native-base";

import ArchHeader from "./../../components/ArchHeader";
import ArchHero from "./../../components/ArchHero";
import { observer, inject } from "mobx-react";

class Qibla extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }

  render() {
    const { getTranslatedString } = this.props.store.strings;
    const event = {
      title: getTranslatedString("Upgrad.ID Event"),
      time: getTranslatedString("Monday, 7th Oct 7:45pm"),
      location: getTranslatedString("Jl. Manunggal 2, Jakarta, Jakarta Raya"),
    };
    const params = this.props.navigation.state.params;
    console.log(this.props.navigation.state);
    return (
      <Container>
        <ArchHeader
          {...this.props}
          title={getTranslatedString("Event Detail")}
          isLoading={this.state.isLoading}
        />
        <Content style={{ backgroundColor: colors.backgroundColor }}>
          {params && (
            <Card
              transparent
              style={{
                marginTop: 0,
                marginLeft: 0,
                marginRight: 0,
                marginBottom: 10,
              }}
            >
              <CardItem cardBody>
                <Thumbnail
                  source={{
                    uri: params.eventData.imageUrl,
                  }}
                  style={{
                    height: 200,
                    borderRadius: 0,
                    width: Dimensions.get("window").width,
                  }}
                />
              </CardItem>
              <CardItem>
                <Body
                  style={{
                    borderBottomWidth: 2,
                    paddingBottom: 10,
                    borderBottomColor: colors.grey,
                  }}
                >
                  <Text
                    style={{
                      fontSize: 25,
                      fontWeight: "bold",
                      marginBottom: 20,
                    }}
                  >
                    {params.eventData.name}
                    {/* {getTranslatedString("Upgrad.ID Event")} */}
                  </Text>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Icon
                      name="time"
                      style={{ fontSize: 20, color: colors.primaryColor }}
                    />
                    <Text style={{ paddingLeft: 5 }}>
                      {`${new Date(
                        params.eventData.eventDate
                      ).toDateString()}, ${params.eventData.eventTime}`}
                      {/* {getTranslatedString("Monday, 7th Oct 7:45pm")} */}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Icon
                      name="pin"
                      style={{
                        fontSize: 20,
                        color: colors.primaryColor,
                        paddingLeft: 2,
                      }}
                    />
                    <Text style={{ paddingLeft: 5 }}>
                      {params.eventData.location}
                    </Text>
                  </View>
                </Body>
              </CardItem>
              <Text
                style={{
                  padding: 10,
                  lineHeight: 20,
                  marginTop: 10,
                }}
              >
                {params.eventData.description}
              </Text>
            </Card>
          )}
        </Content>
      </Container>
    );
  }
}

export default inject("store")(observer(Qibla));
