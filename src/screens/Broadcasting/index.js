import React, { Component } from "react";
import { View, Text, Alert, PermissionsAndroid } from "react-native";
import colors from "./../../assets/colors";
import st from "./../../assets/styles";
import moment from "moment";
import Geolocation from "@react-native-community/geolocation";
import Geocoder from "react-native-geocoding";

import {
  Container,
  Content,
  List,
  ListItem,
  Left,
  Right,
  Body,
  Button,
  Tabs,
  Tab,
  TabHeading
} from "native-base";

import ArchHeader from "./../../components/ArchHeader";

import LiveBroadcast from "./tabs/LiveBroadcast";
import ListBroadcast from "./tabs/ListBroadcast";
import ArchivedBroadcasts from "./tabs/ArchivedBroadcasts";
import { inject, observer } from "mobx-react";

class Broadcasting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    };
  }

  render() {
    const { getTranslatedString } = this.props.store.strings;

    return (
      <Container>
        <ArchHeader
          {...this.props}
          title={getTranslatedString("AUDIO & VIDEO BROADCASTING")}
          isLoading={this.state.isLoading}
          hasTabs={true}
        />
        <Tabs tabBarUnderlineStyle={st.tabs}>
          {global.role === "admin" && (
            <Tab
              tabStyle={st.tab}
              activeTabStyle={st.tabActive}
              textStyle={st.tabText}
              heading={
                <TabHeading style={st.tab}>
                  <Text style={{ color: colors.primaryColor }}>
                    {getTranslatedString("Start")}
                  </Text>
                </TabHeading>
              }
            >
              <LiveBroadcast
                isLoading={this.state.isLoading}
                navigation={this.props.navigation}
              />
            </Tab>
          )}
          <Tab
            tabStyle={st.tab}
            activeTabStyle={st.tabActive}
            textStyle={st.tabText}
            heading={
              <TabHeading style={st.tab}>
                <Text style={{ color: colors.primaryColor }}>
                  {getTranslatedString("List")}
                </Text>
              </TabHeading>
            }
          >
            <ListBroadcast navigation={this.props.navigation} />
          </Tab>
          <Tab
            tabStyle={st.tab}
            activeTabStyle={st.tabActive}
            textStyle={st.tabText}
            heading={
              <TabHeading style={st.tab}>
                <Text style={{ color: colors.primaryColor }}>
                  {getTranslatedString("Archive")}
                </Text>
              </TabHeading>
            }
          >
            <ArchivedBroadcasts navigation={this.props.navigation} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

export default inject("store")(observer(Broadcasting));
