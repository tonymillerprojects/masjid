import React, { Component } from "react";
import {
  View,
  Text,
  Alert,
  TouchableHighlight,
  PermissionsAndroid,
  NativeModules
} from "react-native";
import moment from "moment";
import auth, { firebase } from "@react-native-firebase/auth";
import colors from "./../../../assets/colors";
import { Content, Icon, Item, Input } from "native-base";
import { typeAudio, typeVideo } from "../../BroadcastPlayer/Broadaster";
import { observer, inject } from "mobx-react";
import { Directions } from "react-native-gesture-handler";
const { Agora } = NativeModules;

if (!Agora) {
  throw new Error(
    "Agora load failed in react-native, please check ur compiler environments"
  );
}

const {
  FPS30,
  AudioProfileDefault,
  AudioScenarioDefault,
  Host,
  Adaptative,
  Audience
} = Agora;

async function requestCameraAndAudioPermission() {
  try {
    const granted = await PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.CAMERA,
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO
    ]);

    if (
      granted["android.permission.RECORD_AUDIO"] ===
      PermissionsAndroid.RESULTS.GRANTED
    ) {
      console.log("You can use the audio");
    } else {
      console.log("Audio permission denied");
    }

    if (
      granted["android.permission.CAMERA"] ===
      PermissionsAndroid.RESULTS.GRANTED
    ) {
      console.log("You can use the camera");
    } else {
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
}

class LiveBroadcast extends Component {
  constructor() {
    super();
    this.state = {
      channelName: "",
      nowIs: moment().format("DD MMM YYYY       hh:mm")
    };

    if (Platform.OS === "android") {
      requestCameraAndAudioPermission().then(_ => {});
    }
  }

  render() {
    const name = global.name;
    const role = global.role;
    console.log("role========>>>>>>>>>", role);
    const { getTranslatedString } = this.props.store.strings;
    return (
      <Content
        style={{
          backgroundColor: colors.backgroundColor
        }}
        contentContainerStyle={{
          paddingBottom: 40
        }}
      >
        <View
          style={{
            flex: 1,
            marginTop: 20,
            marginLeft: 20,
            marginRight: 20,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Item regular>
            <Input
              placeholder={getTranslatedString("INSERT LECTURE TITLE")}
              placeholderTextColor={colors.grey}
              onChangeText={channelName => {
                this.setState({ channelName: channelName });
              }}
              value={this.state.channelName}
              maxLength={25}
              style={{ borderColor: "#000", borderWidth: 1 }}
            />
          </Item>
        </View>
        <View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              marginTop: 15
            }}
          >
            <Text style={{ fontSize: 20 }}>{this.state.nowIs}</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              marginTop: 15
            }}
          >
            <Text style={{ fontSize: 20, marginRight: 20 }}>{name}</Text>
            <Text style={{ fontSize: 20, marginLeft: 50 }}>
              {global.defaultMasjidName}
            </Text>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            marginTop: 30,
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row"
          }}
        >
          <TouchableHighlight
            style={{
              ...style.highButton,
              marginTop: 15
            }}
            underlayColor={colors.backgroundColor}
            onPress={() => {
              if (!this.state.channelName.trim()) {
                return Alert.alert(
                  getTranslatedString("Error!"),
                  getTranslatedString("Please enter a channel name")
                );
              }

              const channelName = this.state.channelName
                .trim()
                .replace(/[\W]/gi, "_");

              console.log("channelName to be made: ", channelName);
              this.props.navigation.navigate("BroadcastPlayer", {
                channelName,
                type: typeAudio,
                clientRole: Host
              });

              this.setState({ channelName: "" });
            }}
          >
            <View
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Text
                style={{
                  flex: 1,
                  fontSize: 16,
                  paddingBottom: 20
                }}
              >
                {getTranslatedString("Audio Broadcasting")}
              </Text>
              <View style={style.iconButton}>
                <Icon
                  name="ios-microphone"
                  style={{ fontSize: 60, color: "white" }}
                />
              </View>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            style={style.highButton}
            underlayColor={colors.backgroundColor}
            onPress={() => {
              if (!this.state.channelName.trim()) {
                return Alert.alert(
                  getTranslatedString("Error!"),
                  getTranslatedString("Please enter a channel name")
                );
              }

              const channelName = this.state.channelName
                .trim()
                .replace(/[\W]/gi, "_");

              console.log("channelName to be made: ", channelName);

              this.props.navigation.navigate("BroadcastPlayer", {
                channelName,
                type: typeVideo,
                clientRole: Host
              });

              this.setState({ channelName: "" });
            }}
          >
            <View
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
                marginTop: 15
              }}
            >
              <Text
                style={{
                  flex: 1,
                  fontSize: 16,
                  paddingBottom: 15
                }}
              >
                {getTranslatedString("VIDEO BROADCASTING")}
              </Text>
              <View style={style.iconButton}>
                <Icon
                  name="ios-videocam"
                  style={{ fontSize: 60, color: "white" }}
                />
              </View>
            </View>
          </TouchableHighlight>
        </View>
      </Content>
    );
  }
}

const style = {
  highButton: {
    flex: 1
  },
  iconButton: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.primaryColor,
    width: 120,
    height: 120,
    padding: 15,
    borderRadius: 60
  },
  icon: {
    flex: 1,
    width: 45,
    height: undefined
  },
  menuText: {
    alignSelf: "center",
    fontSize: 15,
    marginTop: 10
  }
};

export default inject("store")(observer(LiveBroadcast));
