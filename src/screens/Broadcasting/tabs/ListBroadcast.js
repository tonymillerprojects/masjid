import React, { Component } from "react";
import { Text, Alert, FlatList, NativeModules } from "react-native";
import st from "./../../../assets/styles";
import colors from "./../../../assets/colors";

import {
	Content,
	List,
	ListItem,
	Left,
	Thumbnail,
	Right,
	Body,
	Button,
	Icon,
	View
} from "native-base";
import database from "@react-native-firebase/database";
import { inject, observer } from "mobx-react";

const { Agora } = NativeModules;

if (!Agora) {
	throw new Error(
		"Agora load failed in react-native, please check ur compiler environments"
	);
}

const {
	FPS30,
	AudioProfileDefault,
	AudioScenarioDefault,
	Host,
	Adaptative,
	Audience
} = Agora;

class ListBroadcast extends Component {
	constructor(props) {
		super(props);
		this.state = {
			numbers: [1, 2, 3, 4, 5, 6],
			broadcastList: []
		};
	}

	componentDidMount() {
		database()
			.ref("broadcast")
			.on("child_added", snapshot => {
				console.log("Firebase", snapshot.val());
				const value = snapshot.val();
				if (value !== null) {
					this.addBroadcastItem(value);
				}
			});

		database()
			.ref("broadcast")
			.on("child_removed", snapshot => {
				console.log("Firebase", snapshot.val());
				const value = snapshot.val();
				if (value !== null) {
					this.removeBroadcastItem(value);
				}
			});
		database()
			.ref("broadcast")
			.on("child_changed", snapshot => {
				console.log("Firebase", snapshot.val());
				const value = snapshot.val();
				if (value !== null) {
					this.removeBroadcastItem(value);
					this.addBroadcastItem(value);
				}
			});
	}

	removeBroadcastItem = item => {
		this.setState({
			broadcastList: this.state.broadcastList.filter(i => i.name !== item.name)
		});
	};
	addBroadcastItem = item => {
		if (!item.isEnd)
			this.setState({ broadcastList: [...this.state.broadcastList, item] });
	};

	playBroadcast = item => {
		this.props.navigation.navigate("BroadcastPlayer", {
			channelId: item.channelId,
			type: item.type,
			clientRole: Audience
		});
	};

	renderItem = ({ item }) => {
		return (
			<View
				style={{
					flex: 1,
					flexDirection: "row",
					padding: 10,
					borderBottomWidth: 1
				}}
			>
				<View style={{ flex: 3 }}>
					<Text style={st.txtLarge} numberOfLines={1}>
						{item.name} - {item.type}
					</Text>
					<View
						style={{
							flex: 1,
							flexDirection: "row",
							alignItems: "center",
							padding: 5
						}}
					>
						<Icon
							name="md-people"
							style={{
								fontSize: 20,
								color: colors.grey,
								marginEnd: 8
							}}
						/>
						<Text>{item.peers}</Text>
					</View>
				</View>
				<View style={{ flex: 1, alignItems: "flex-end" }}>
					<Button transparent onPress={() => this.playBroadcast(item)}>
						<Icon
							name="md-play"
							style={{ fontSize: 30, color: colors.primaryColor }}
						/>
					</Button>
				</View>
			</View>
		);
	};

	renderListEmptyContainer = () => {
		const { getTranslatedString } = this.props.store.strings;

		return (
			<View
				style={{
					marginTop: 20,
					alignItems: 'center',
					justifyContent: 'center'
				}}
			>
				<Text
					style={{
						fontSize: 16
					}}
				>
					{getTranslatedString('There are currently no live broadcasts')}
				</Text>
			</View>
		);
	};

	render() {
		// const getListofBroadcast = () => {
		//   this.state.numbers.maps(index => (
		//     <ListItem avatar key={index} style={{ marginTop: 5, marginBottom: 10 }}>
		//       <Left>
		//         <Thumbnail square source={{ uri: icon }} />
		//       </Left>
		//       <Body>
		//         <Text style={st.txtLarge} numberOfLines={1}>Masjid Name</Text>
		//         <Text note><Icon name='md-people' style={{fontSize: 20, color: colors.grey, marginTop: 15, marginRight: 10}} />10</Text>
		//       </Body>
		//       <Right>
		//         <Button transparent onPress={() => this.playBroadcast()}>
		//           <Icon name='md-play' style={{fontSize: 30, color: colors.primaryColor}} />
		//         </Button>
		//       </Right>
		//     </ListItem>
		//   ))
		// }

		return (
			<Content style={{ backgroundColor: colors.backgroundColor }}>
				<FlatList
					data={this.state.broadcastList}
					renderItem={this.renderItem}
					keyExtractor={item => item.name}
					ListEmptyComponent={this.renderListEmptyContainer}
				/>
			</Content>
		);
	}
}

export default inject('store')(observer(ListBroadcast));
