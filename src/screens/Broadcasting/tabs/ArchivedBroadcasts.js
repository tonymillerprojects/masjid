import React, { Component } from "react";
import { Text, FlatList, Image, TouchableOpacity } from "react-native";
import moment from 'moment';
import st from "./../../../assets/styles";
import colors from "./../../../assets/colors";

import { Content, View } from "native-base";
import database from "@react-native-firebase/database";
import { inject, observer } from "mobx-react";

class ArchivedBroadcasts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            archivedBroadcastList: []
        };
    }

    componentDidMount() {
        database()
            .ref("broadcast")
            .on('value', snapshot => {
                // console.log("\n\nFirebase archived data: ", JSON.stringify(snapshot.val(), null, 2));

                const values = snapshot.val();

                if (values !== null) {
                    this.addBroadcastItem(values);
                }
            });
    }

    addBroadcastItem = item => {
        const updatedBrodcastArray = [];

        Object.keys(item).forEach((key) => {
            if (item[key].isEnd) {
                updatedBrodcastArray.push(item[key])
            }
        });

        // console.log('updatedBrodcastArray', JSON.stringify(updatedBrodcastArray, null, 2))
        updatedBrodcastArray.sort((a, b) => new Date(b.startTime) - new Date(a.startTime));

        this.setState({
            archivedBroadcastList: updatedBrodcastArray
        });
    };

    renderListEmptyContainer = () => {
        const { getTranslatedString } = this.props.store.strings;

        return (
            <View
                style={{
                    marginTop: 20,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Text
                    style={{
                        fontSize: 16
                    }}
                >
                    {getTranslatedString('No Archives Found')}
                </Text>
            </View>
        );
    };

    renderItem = ({ item }) => {
        const { getTranslatedString } = this.props.store.strings;
        let duration = 0;
        let startDate = null;

        if (item.startTime && item.endTime) {
            duration = moment.utc(moment(item.endTime).diff(item.startTime)).format('HH:mm:ss')
        }

        if (item.startTime) {
            startDate = moment(item.startTime).format('DD MMM YYYY')
        }

        return (
            <View
                style={{
                    flex: 1,
                    flexDirection: "row",
                    padding: 10,
                    borderBottomWidth: 1
                }}
            >
                <View style={{ flex: 3 }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            flex: 1,

                        }}
                    >
                        <Text
                            style={{ ...st.txtLarge, flex: 1 }}
                            numberOfLines={1}
                        >
                            {item.name}
                        </Text>

                        <Text
                            style={st.txtLarge}
                            numberOfLines={1}
                        >
                            - {item.type}
                        </Text>
                        <Text style={{marginLeft:7}}>
                        {startDate}
                         </Text>


                    </View>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: "row",
                            justifyContent: "space-between",

                        }}
                    >
                        <Text style={{marginRight:0}}>
                            {getTranslatedString('Duration:')} {String(duration)}
                        </Text>
                        <TouchableOpacity style={{alignContent: 'center', marginRight: 20}}>
                         <Image source={require('../../../assets/images/icons/share.png')} />
                        </TouchableOpacity>

                    </View>
                </View>
            </View>
        );
    };

    render() {
        return (
            <Content style={{ backgroundColor: colors.backgroundColor }}>
                <FlatList
                    data={this.state.archivedBroadcastList}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.channelId}
                    ListEmptyComponent={this.renderListEmptyContainer}
                />
            </Content>
        );
    }
}

export default inject('store')(observer(ArchivedBroadcasts));
