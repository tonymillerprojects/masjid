import React, { Component } from 'react'
import { Container, Content, Spinner } from 'native-base'
import Geocoder from 'react-native-geocoding'
import auth from '@react-native-firebase/auth'

import colors from '../../assets/colors'

class LoadingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bUser: false,
    }
  }
  // check if user is already logged in
  componentDidMount() {
    Geocoder.init('AIzaSyBC0KVa-UF8wZMv9JRMUmbflqVAVP4BktI', { language: 'en' })
    auth().onAuthStateChanged((user) => {
      if (user) {
        // console.log("++++++++++++",user);
        global.name = user.displayName;
        global.email = user.email;
        global.photoUrl = user.photoURL;
        global.uid = user.uid;
        global.id = user.uid;
        global.role = user.role;
        this.setState({ bUser: true })
      }
      this.props.navigation.navigate(user ? 'MainApp' : 'LoginStack');
    });
  }

  render() {
    if (this.state.bUser)
      return (
        <Container >

        </Container>
      )
    return (
      <Container style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Spinner color={colors.primaryColor} />
      </Container>
    );
  }
}

export default LoadingScreen
