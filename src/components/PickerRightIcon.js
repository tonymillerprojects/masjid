import React from 'react';
import { Image, StyleSheet } from 'react-native';

import { images } from '../assets/images';

const PickerRightIcon = () => (
    <Image
        source={images.ic_down}
        style={styles.downArrow}
    />
);

const styles = StyleSheet.create({
    downArrow: {
        transform: [{ scale: 1.5 }]
    }
});

export { PickerRightIcon };
