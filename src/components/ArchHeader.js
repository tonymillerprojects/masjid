import React, { Component } from "react";
import { Header, Body, Title, Left, Right, Spinner, Icon } from "native-base";
import { TouchableOpacity, View, Text } from "react-native";
import st from "./../assets/styles";
import colors from "./../assets/colors";
import AntDesign from "react-native-vector-icons/AntDesign";

class ArchHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderRight = () => {
    if (this.props.isLoading) {
      return <Spinner size="small" color="white" style={{ marginLeft: 10 }} />;
    }
    return null;
  };

  render() {
    return (
      <Header
        style={{ backgroundColor: colors.primaryColor }}
        androidStatusBarColor={colors.secondaryColor}
        hasTabs={this.props.hasTabs ? true : false}
      >
        <Left style={{ flex: 1 }}>
          <TouchableOpacity
            style={{}}
            onPress={() => this.props.navigation.openDrawer()}
          >
            <AntDesign name="bars" style={{ color: "white", fontSize: 30 }} />
          </TouchableOpacity>
        </Left>
        <Body style={st.centerTitle}>
          <Title style={st.headerTitle}>{this.props.title}</Title>
        </Body>

        {this.props.renderCenterRightButton
          ? this.props.renderCenterRightButton()
          : null}

        {this.props.onClickResetDefault ? (
          <Right>
            <TouchableOpacity
              style={{}}
              onPress={() => this.props.onClickResetDefault()}
            >
              <Icon name="sync" style={{ color: "white", fontSize: 30 }} />
            </TouchableOpacity>
          </Right>
        ) : (
          <Right
            style={{ flex: 1, flexDirection: "row", alignItems: "center" }}
            onPress={() => this.props.onpress}
          >
            {this.renderRight()}
          </Right>
        )}
      </Header>
    );
  }
}

export default ArchHeader;
