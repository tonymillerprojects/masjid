import React, { Component } from 'react'
import { View, Text, Platform, TouchableOpacity } from 'react-native'
import {
  Button,
} from 'native-base'
import st from './../assets/styles'
import iosStyle from './../assets/iosStyles'
import colors from './../assets/colors'

class SocialButton extends Component {
  render() {
    const { navigation, text, } = this.props

    if (Platform.OS === 'ios') {
      return (
        <View style={iosStyle.btnView1}>
          <Button rounded light style={iosStyle.btnRounded1} onPress={
            this.props.onpress ? this.props.onpress() : () => navigation.navigate('MainApp')
            // alert('hello')
          }>
            <Text style={{ color: colors.primaryColor, fontSize: 16, fontWeight: 'bold', alignSelf: 'center' }}>{text}</Text>
          </Button>
        </View>
      )
    }

    // return (
    //   <View style={{ alignSelf: 'stretch', marginTop: 10 }}>
    //     <Button rounded style={st.btnRounded} onPress={() => navigation.navigate('MainApp')}>
    //       <Text style={{ color: colors.primaryColor, fontSize: 25, fontWeight: 'bold', alignSelf: 'center' }}>{text}</Text>
    //     </Button>
    //   </View>
    // )
    return (
      <View style={iosStyle.btnView1}>
        <TouchableOpacity style={st.btnRounded1} onPress={
          this.props.onpress ? this.props.onpress : () => navigation.navigate('MainApp')
          // navigation.navigate('MainApp')
        }>
          <Text style={{ color: colors.primaryColor, fontSize: 16, fontWeight: 'bold', alignSelf: 'center' }}>{text}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

export default SocialButton
