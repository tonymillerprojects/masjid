/* eslint-disable prettier/prettier */
import storeModel from './../models';
import { strings } from '../../utils/strings';

const store = storeModel.create({
  userStore: {
    uid: '',
    email: '',
    username: '',
    password: '',
    photoUrl : '',
    error: '',
    loading1: false,
    loading2: false,
  },
  strings: { strings }
});

export default store;
