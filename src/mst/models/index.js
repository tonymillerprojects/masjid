/* eslint-disable prettier/prettier */
import { types } from 'mobx-state-tree';

import userStore from './user';
import strings from './strings';

const storeModel = types.model('Store', {
  userStore: userStore,
  strings
});

export default storeModel;
