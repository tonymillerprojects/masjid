import { types } from 'mobx-state-tree';

import stringActions from '../actions/stringActions';

const singleLanguage = types.model('language', {
    original: types.string,
    translated: types.string
});

const stringsModel = types
    .model('strings', {
        strings: types.array(singleLanguage)
    })
    .views((self) => ({
        getTranslatedString(string) {
            const requestedString = self.strings.find((s) => s.original.toLowerCase() ===  string.toLowerCase())
    
            // console.log('requestedString: ', requestedString)
            if (requestedString) {
                return requestedString.translated;
            }

            return '';
        }
    }))
    .actions(stringActions);

export default stringsModel;
