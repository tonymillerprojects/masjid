const stringActions = self => ({
    setStrings(strings) {
        self.strings = strings;
    },
});

export default stringActions;
