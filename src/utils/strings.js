const strings = [
  {
    original: "Broadcast",
    translated: "Broadcast"
  },
  {
    original: "AUDIO & VIDEO BROADCASTING",
    translated: "AUDIO & VIDEO BROADCASTING"
  },
  {
    original: "Audio & Video Broadcasting",
    translated: "Audio & Video Broadcasting"
  },
  {
    original: "Prayer",
    translated: "Prayer"
  },
  {
    original: "QIBLA LOCATER",
    translated: "QIBLA LOCATER"
  },
  {
    original: "MASJID LOCATION",
    translated: "MASJID LOCATION"
  },
  {
    original: "Quran",
    translated: "Quran"
  },
  {
    original: "MASJID EVENTS & ANNOUNCEMENTS",
    translated: "MASJID EVENTS & ANNOUNCEMENTS"
  },
  {
    original: "Home",
    translated: "Home"
  },
  {
    original: "SALAAH TIMES",
    translated: "SALAAH TIMES"
  },
  {
    original: "Set Masjid",
    translated: "Set Masjid"
  },
  {
    original:
      "In order for you to view the salah times that is closest to you. Please set your default masjid below",
    translated:
      "In order for you to view the salah times that is closest to you. Please set your default masjid below"
  },
  {
    original: "Salaah Time",
    translated: "Salaah Time"
  },
  {
    original: "Qibla Locater",
    translated: "Qibla Locater"
  },
  {
    original: "Masjid location",
    translated: "Masjid location"
  },
  {
    original: "Subscription",
    translated: "Subscription"
  },
  {
    original: "Masjid Events & Announcements",
    translated: "Masjid Events & Announcements"
  },
  {
    original: "Sign Out",
    translated: "Sign Out"
  },
  {
    original: "Start",
    translated: "Start"
  },
  {
    original: "List",
    translated: "List"
  },
  {
    original: "Archive",
    translated: "Archive"
  },
  {
    original: "No Archives Found",
    translated: "No Archives Found"
  },
  {
    original: "Duration:",
    translated: "Duration:"
  },
  {
    original: "There are currently no live broadcasts",
    translated: "There are currently no live broadcasts"
  },
  {
    original: "INSERT LECTURE TITLE",
    translated: "INSERT LECTURE TITLE"
  },
  {
    original: "Error!",
    translated: "Error!"
  },
  {
    original: "Please enter a channel name",
    translated: "Please enter a channel name"
  },
  {
    original: "Broadcast Name(max 25 characters)",
    translated: "Broadcast Name(max 25 characters)"
  },
  {
    original: "VIDEO BROADCASTING",
    translated: "VIDEO BROADCASTING"
  },
  {
    original: "AUDIO BROADCASTING",
    translated: "AUDIO BROADCASTING"
  },
  {
    original: "Cannot start broadcating now. Please try again later.",
    translated: "Cannot start broadcating now. Please try again later."
  },
  {
    original: "Ok",
    translated: "Ok"
  },
  {
    original: "Name :",
    translated: "Name :"
  },
  {
    original: "Upgrad.ID Event",
    translated: "Upgrad.ID Event"
  },
  {
    original: "Monday, 7th Oct 7:45pm",
    translated: "Monday, 7th Oct 7:45pm"
  },
  {
    original: "Jl. Manunggal 2, Jakarta, Jakarta Raya",
    translated: "Jl. Manunggal 2, Jakarta, Jakarta Raya"
  },
  {
    original: "Event Detail",
    translated: "Event Detail"
  },
  {
    original:
      "In the following example, the nested title and body text will inherit the fontFamily from styles.baseText, but the title provides its own additional styles. The title and body will stack on top of each other on account of the literal newlines.",
    translated:
      "In the following example, the nested title and body text will inherit the fontFamily from styles.baseText, but the title provides its own additional styles. The title and body will stack on top of each other on account of the literal newlines."
  },
  {
    original: "Subscriptions",
    translated: "Subscriptions"
  },
  {
    original: "You are subscribed to monthly subscription of $1 for 1 month.",
    translated: "You are subscribed to monthly subscription of $1 for 1 month."
  },
  {
    original: "Currently you do not have any subscription",
    translated: "Currently you do not have any subscription"
  },
  {
    original: "Get subscription for 1 month",
    translated: "Get subscription for 1 month"
  },
  {
    original: "Search",
    translated: "Search"
  },
  {
    original: "Settings",
    translated: "Settings"
  }
];

export { strings };
