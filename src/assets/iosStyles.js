import { StyleSheet } from 'react-native'
import colors from './colors'
const iosStyles = {
    btnView: {
        alignSelf: 'stretch',
        marginLeft: 10,
        marginRight: 10,
    },
    btnView1: {
        marginTop: 10,
    },
    btnRounded: {
        height:50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'    
    },
    btnRounded1: {
        width:160,
        height:50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'    
    }
}

export default iosStyles