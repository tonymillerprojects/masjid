/* eslint-disable prettier/prettier */
const colors = {
  primaryColor: "#006b2d",
  secondaryColor: "#006b2d",
  backgroundColor: "#ffffff",
  homeButtonBgColor: "#e5e5e5",
  grey: "#979797",
  black1: "rgba(0,0,0,0.4)"
};

export default colors;
