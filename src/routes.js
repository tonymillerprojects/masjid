import React from "react";
import { Dimensions } from "react-native";
import { createDrawerNavigator } from "react-navigation-drawer";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import LoadingScreen from "./screens/Loading";
import LoginScreen from "./screens/Login";
import RegisterScreen from "./screens/Register";
import ResetPassword from "./screens/ResetPassword";
import WelcomeScreen from "./screens/Welcome";
import SettingsScreen from "./screens/Settings";
import QuranScreen from "./screens/Quran";
import QuranDetailScreen from "./screens/QuranDetail";
import PrayerScreen from "./screens/Prayer";
import PrayerTimesScreen from "./screens/PrayerTimes";
import MasjidScreen from "./screens/Masjid";
import QiblaScreen from "./screens/Qibla";
import BroadcastingScreen from "./screens/Broadcasting";
import BroadcastPlayerScreen from "./screens/BroadcastPlayer";
import EventScreen from "./screens/Event";
import EventDetailScreen from "./screens/EventDetail";

import { Drawer } from "./screens/Drawer";
import SubscriptionScreen from "./screens/SubscriptionScreen";
const WINDOW_WIDTH = Dimensions.get("window").width;

const LoginStack = createStackNavigator(
  {
    Login: LoginScreen,
    Register: RegisterScreen,
    // Login : WelcomeScreen,
    // Home: WelcomeScreen,
    ResetPassword: ResetPassword,
  },
  {
    headerMode: "none",
  }
);

const MainStack = createStackNavigator(
  {
    Welcome: WelcomeScreen,
    Settings: SettingsScreen,
    Broadcasting: BroadcastingScreen,
    // Prayer: PrayerScreen,
    Prayer: PrayerTimesScreen,
    Qibla: QiblaScreen,
    Masjid: MasjidScreen,
    Quran: QuranScreen,
    Event: EventScreen,

    QuranDetail: QuranDetailScreen,
    BroadcastPlayer: BroadcastPlayerScreen,
    EventDetail: EventDetailScreen,
    Subscription: SubscriptionScreen,
  },
  {
    headerMode: "none",
    defaultNavigationOptions: {
      swipeEnabled: true,
    },
  }
);

const MainApp = createDrawerNavigator(
  {
    MainStack,
  },
  {
    contentComponent: (props) => <Drawer {...props} />,
    drawerWidth: WINDOW_WIDTH * 0.8,
    // swipeEnabled: true,
  }
);

export default createAppContainer(
  createSwitchNavigator({
    LoadingScreen,
    LoginStack,
    MainApp,
  })
);
